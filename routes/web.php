<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [
    'as' => 'index',
    'uses' => 'IndexController@index',
]);;
Auth::routes();
Route::group(['middleware' => ['permission:worker']], function () {
    Route::resource('dashboard', 'DashBoardController');
    Route::get('/getpiedata/{id}', 'DashBoardController@getPie')->name('Data');
    Route::get('/getbardata/{id}', 'DashBoardController@getbar')->name('Data');
    Route::get('/activity', 'DashBoardController@activity')->name('Profile');
    Route::get('/workers/documents/{id}/edit', [
        'as' => 'workers.document.edit',
        'uses' => 'WorkerController@documents',
    ]);

    Route::post('/workers/permanent/delete/', [
        'as' => 'workers.delete.permanent',
        'uses' => 'WorkerController@deleteworker',
    ]);
    Route::post('/documents', [
        'as' => 'workers.document.updatedoc',
        'uses' => 'WorkerController@updatedoc',
    ]);
    Route::post('/workers/documents/download/', [
        'as' => 'workers.document.download',
        'uses' => 'WorkerDocsController@document_download',
    ]);
    Route::resource('workers', 'WorkerController');

});
Route::group(['middleware' => ['permission:administrator']], function () {
    Route::resource('client', 'ClientController');
    Route::post('client/delete', [
        'as' => 'client.delete',
        'uses' => 'ClientController@delete',
    ]);
    Route::post('client/state', [
        'as' => 'client.state',
        'uses' => 'ClientController@state',
    ]);
    Route::post('client/event/store', [
        'as' => 'client.store.event',
        'uses' => 'ClientController@storeEvent',
    ]);
    Route::get('client/{id}/create', [
        'as' => 'client.create.event',
        'uses' => 'ClientController@createEvent',
    ]);
    Route::get('client/event/{id}', [
        'as' => 'client.show.event',
        'uses' => 'ClientController@showEvent',
    ]);
    Route::post('client/event/delete', [
        'as' => 'client.delete.event',
        'uses' => 'ClientController@deleteEvent',
    ]);
    Route::post('client/event/state', [
        'as' => 'client.event.state',
        'uses' => 'ClientController@stateEvent',
    ]);
    Route::get('client/event/{id}/addworker/', [
        'as' => 'client.event.addWorker',
        'uses' => 'ClientController@addEventWorker',
    ]);

    Route::resource('roles', 'RoleController');
    Route::resource('users', 'UserController');
    Route::get('/users/enterprise/new', [
        'as' => 'users.Enterprise',
        'uses' => 'UserController@Enterprise',
    ]);
    Route::get('/users/enterprise/change', [
        'as' => 'users.Enterprise.edit',
        'uses' => 'UserController@EnterpriseEdit',
    ]);
    Route::get('/admin/{id}/{date}/entfilter', [
        'as' => 'dashboard.company.filter',
        'uses' => 'AdminController@enterfilter',
    ]);
    Route::get('/admin/{id}/{date}/workfilter', [
        'as' => 'dashboard.worker.filter',
        'uses' => 'AdminController@workfilter',
    ]);

    Route::post('/users/enterprise', [
        'as' => 'users.store_enterprise',
        'uses' => 'UserController@PostEnterprise',
    ]);
    Route::post('/users/enterpriseedit', [
        'as' => 'users.store_enterprise_edit',
        'uses' => 'UserController@PostEnterpriseEdit',
    ]);
    Route::post('/admin-filter', [
        'as' => 'dashboard.filter',
        'uses' => 'AdminController@filter',
    ]);
    Route::resource('workers', 'WorkerController');
    Route::resource('admin', 'AdminController');
    Route::resource('otherworkers', 'OtherworkersController');
    Route::get('company/{id?}/details', [
        'as' => 'companies.info',
        'uses' => 'CompaniesController@details',
    ]);
    Route::post('company/details', [
        'as' => 'companies.info.update',
        'uses' => 'CompaniesController@detailsUpdate',
    ]);

    Route::resource('companies', 'CompaniesController');
    Route::get('company/{id?}/filter', [
        'as' => 'company.filter',
        'uses' => 'CompanyDocController@filter',
    ]);
    Route::post('/workers/documents/downloadadmin/', [
        'as' => 'workers.document.downloadadmin',
        'uses' => 'WorkerDocsController@document_download_admin',
    ]);
    Route::post('/company/delete', [
        'as' => 'company.delete',
        'uses' => 'CompanyDocController@deleteType',
    ]);
    Route::post('/company/create', [
        'as' => 'company.document.typecreate',
        'uses' => 'CompanyDocController@POST_type',
    ]);
    Route::get('/company/delete', [
        'as' => 'company.deleteview',
        'uses' => 'CompanyDocController@deleteview',
    ]);
    Route::post('/workers/type/delete', [
        'as' => 'workers.delete',
        'uses' => 'WorkerDocsController@deleteType',
    ]);
    Route::get('/workers/type/delete', [
        'as' => 'workers.deleteview',
        'uses' => 'WorkerDocsController@deleteview',
    ]);
    Route::get('/workers/documents/new', [
        'as' => 'workers.document.type',
        'uses' => 'WorkerController@newDocType',
    ]);
    Route::post('/documents/typecreate', [
        'as' => 'workers.document.POST_type',
        'uses' => 'WorkerController@POST_type',
    ]);
    Route::post('/worker/approve', [
        'as' => 'worker.document.approve',
        'uses' => 'WorkerDocsController@approve',
    ]);
    Route::post('/worker/deny', [
        'as' => 'worker.document.deny',
        'uses' => 'WorkerDocsController@deny',
    ]);
    Route::post('/worker/delete', [
        'as' => 'worker.document.delete',
        'uses' => 'WorkerDocsController@deletedoc',
    ]);
    Route::post('/company/approve', [
        'as' => 'company.document.approve',
        'uses' => 'CompanyDocController@approve',
    ]);
    Route::post('/company/deny', [
        'as' => 'company.document.deny',
        'uses' => 'CompanyDocController@deny',
    ]);
});
Route::group(['middleware' => ['permission:manager']], function () {
    Route::resource('company', 'CompanyDocController');
    Route::post('/company/getfile', [
        'as' => 'company.document.get',
        'uses' => 'CompanyDocController@POST_getFiles',
    ]);
    Route::post('/company/uploadfile', [
        'as' => 'company.document.set',
        'uses' => 'CompanyDocController@POST_upload',
    ]);
    Route::post('/event/file/download', [
        'as' => 'event.document.download',
        'uses' => 'ClientController@getDownload',
    ]);
    Route::post('/company/getDocs', [
        'as' => 'worker.document.get',
        'uses' => 'WorkerDocsController@getWorkerDocs',
    ]);
    Route::post('/client/event/worker/add', [
        'as' => 'client.event.store.worker',
        'uses' => 'ClientController@storeEventWorker',
    ]);
    Route::get('/Companyfileupload', [
        'as' => 'company.document.upload',
        'uses' => 'CompanyDocController@upload',
    ]);
    Route::get('/client/event/{id}/worker/{workerID}', [
        'as' => 'client.event.worker.show',
        'uses' => 'ClientController@showWorkerDocs',
    ]);
    Route::get('company/{id?}/filter', [
        'as' => 'company.filter',
        'uses' => 'CompanyDocController@filter',
    ]);
    Route::get('workers/{id?}/filter', [
        'as' => 'workers.filter',
        'uses' => 'WorkerDocsController@filter',
    ]);

    Route::resource('workers', 'WorkerController');

});
Route::group(['middleware' => ['permission:client']], function () {
    Route::resource('clientevents', 'ClientEventsController');
    Route::get('clientevents/{id}/{workerID}', [
        'as' => 'clientevents.worker',
        'uses' => 'ClientEventsController@worker',
    ]);
    Route::post('clientevents/filedownload', [
        'as' => 'clientevents.download',
        'uses' => 'ClientController@getDownload',
    ]);
});