<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

/**
 * @property int $enterpriseID
 * @property string $enterpriseName
 * @property string $enterpriseAddress
 * @property string $enterpriseNif
 * @property string $created_at
 * @property string $updated_at
 * @property int $enterpriseTelephone
 */
class Enterprise extends Model
{
    /**
     * The table associated with the model.
     * @var string
     */
    protected $table = 'enterprise';

    /**
     * The primary key for the model.
     * @var string
     */
    protected $primaryKey = 'enterpriseID';

    /**
     * @var array
     */
    protected $fillable = [
        'enterpriseName', 'enterpriseAddress', 'enterpriseNif', 'created_at', 'updated_at', 'enterpriseTelephone',
    ];

    public function workers()
    {
        return $this->hasMany(User::class, 'fk_enterpriseID', 'enterpriseID');
    }

}
