<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $WorkertypeID
 * @property string $typeName
 * @property boolean $expires
 * @property string $created_at
 * @property string $updated_at
 */
class Workerdocs_type extends Model
{
    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'WorkertypeID';

    /**
     * @var array
     */
    protected $fillable = ['typeName', 'expires', 'created_at', 'updated_at'];

}
