<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @method static create(array $array)
 */
class WorkerDocsTypes extends Model
{
    protected $table = 'workerdocs_types';

    protected $primaryKey = 'WorkertypeID';

    protected $fillable = [
        'typeName',
        'expires',
    ];
}
