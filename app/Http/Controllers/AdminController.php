<?php

namespace App\Http\Controllers;

use App\CompanyDocs;
use App\Enterprise;
use App\WorkerDocs;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $filter = 0;
        $data = Enterprise::all();

        return view('admin.admin-panel')
            ->with('data', $data)
            ->with('filter', $filter);
    }

    public function filter(Request $request)
    {
        $date = $request['expire_date'];
        $filter = 1;

        $data = Enterprise::all();

        return view('admin.admin-panel')
            ->with('data', $data)
            ->with('filter', $filter)
            ->with('date', $date);
    }

    public function enterfilter($id, $date)
    {
        $filter = 0;

        $user = auth()->user();
        $company = Enterprise::find($id);
        $files = CompanyDocs::all()
            ->where('company_id', '=', $id)
            ->where('expires_at', '<=', $date)
            ->where('expires_at', '!=', NULL);

        return view('admin.admin-filter')
            ->with('docs', $files)
            ->with('user', $user)
            ->with('filter', $filter)
            ->with('company', $company);
    }

    public function workfilter($id, $date)
    {
        $filter = 0;
        $user = auth()->user();

        $files = WorkerDocs::all()
            ->where('company_id', '=', $id)
            ->where('expires_at', '<=', $date)
            ->where('expires_at', '!=', NULL);

        return view('admin.admin-filter')
            ->with('docs', $files)
            ->with('user', $user)
            ->with('filter', $filter);
    }

    /**
     * Show the form for creating a new resource.
     * @return void
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     *
     * @return void
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return void
     */
    public function destroy($id)
    {
        //
    }
}
