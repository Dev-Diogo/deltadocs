<?php

namespace App\Http\Controllers;

use App\Enterprise;
use App\Mail\EmailSender;
use App\Model_Roles;
use App\User;
use App\WorkerDocs;
use App\Workerdocs_type;
use App\WorkerDocsTypes;
use Illuminate\Http\Request;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Storage;
use stdClass;

class WorkerController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Model_Roles[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index()
    {
        $filter = 0;

        $data = User::join('model_has_roles', 'model_has_roles.model_id', '=', 'users.id')
            ->select('users.*', 'model_has_roles.role_id')
            ->where('model_has_roles.role_id', '=', '3')
            ->get();

        $user = auth()->user();

        return view('workers.index', compact('data'))
            ->with('user', $user)
            ->with('filter', $filter);
    }

    /**
     * Show the form for creating a new resource.
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = auth()->user();
        $enterprise = Enterprise::all();

        return view('workers.create')
            ->with('enterprise', $enterprise)
            ->with('user', $user);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|same:confirm-password',
            'fk_enterpriseID' => 'required',
        ]);

        $input = $request->all();

        $input['password'] = Hash::make($input['password']);

        $user = User::create($input);

        $user->assignRole('worker');

        WorkerController::sendEmail($request->input('email'));

        return redirect()
            ->route('workers.index')
            ->with('success', 'Utilizador Criado Com Sucesso');
    }

    public function sendEmail($email_address)
    {
        $credentials = ['email' => $email_address];
        $response = Password::sendResetLink($credentials, function (Message $message) {
            $message->subject($this->getEmailSubject());
        });

        switch ($response) {
            case Password::RESET_LINK_SENT:

                return redirect()
                    ->back()
                    ->with('status', trans($response));

            case Password::INVALID_USER:

                return redirect()
                    ->back()
                    ->withErrors(['email' => trans($response)]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return string
     */
    public function show($id)
    {
        $user = User::find($id);

        $docs = WorkerDocs::join('workerdocs_types', 'workerdocs_types.WorkertypeID', '=', 'workerdocs.fk_typeID')
            ->select('workerdocs.*', 'workerdocs_types.*')
            ->where('worker_id', '=', $id)
            ->orderBy('typeName', 'desc')
            ->get();

        return view('workers.show')
            ->with('user', $user)
            ->with('docs', $docs)
            ->with('id', $id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return string
     */
    public function edit($id)
    {
        $data = Model_Roles::all()
            ->where('model_id', '=', $id);

        $key = array_keys(json_decode($data, TRUE));

        $key = $key[0];

        $role_id = $data[$key]["role_id"];

        if ($role_id == '3') {
            $user = User::find($id);
            $enterprise = Enterprise::all();

            return view('workers.edit', compact('user'))->with('enterprise', $enterprise);
        } else {


            return redirect()->route('workers.index');
        }
    }

    public function documents($id)
    {
        $user = auth()->user();
        $types = WorkerDocsTypes::all();

        return view('workers.documents', compact('user'))
            ->with('id', $id)
            ->with('types', $types);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email,' . $id,
            'password' => 'same:confirm-password',
        ]);

        $input = $request->all();

        if (!empty($input['password'])) {

            $input['password'] = Hash::make($input['password']);

        } else {

            $input = array_except($input, ['password']);
        }
        $user = User::find($id);
        $user->update($input);

        return redirect()->route('workers.index')
            ->with('success', 'Utilizador Atualizado Com Sucesso');
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updatedoc(Request $request)
    {
        $this->validate($request, [
            'doc' => 'mimes:pdf',
        ]);

        $doc = $request->file('doc');

        $worker_id = $request['worker_id'];



//        $original = explode('.', $original);
//
//        $original = $original['0'];

        $type = explode('/', $request['typename']);
        $original = Workerdocs_type::find($type['0'])->typeName;
        $count = WorkerDocs::all()
            ->where('company_id', '=', auth()->user()->enterprise->enterpriseID)
            ->where('fk_typeID', '=', $type['0'])
            ->where('worker_id', '=', $worker_id)
            ->count();

        if ($count > 0) {

            $file = WorkerDocs::all()
                ->where('company_id', '=', auth()->user()->enterprise->enterpriseID)
                ->where('fk_typeID', '=', $type['0'])
                ->where('worker_id', '=', $worker_id)
                ->first();

            WorkerDocs::destroy($file['worker_documentID']);

            Storage::delete('/documents/company/' . $request['enterprise_id'] . '/workers/' . $worker_id . '/' . $file['documentName'] . '.' . $file['document_extension']);
        }
        $docname = $doc->storePublicly('/documents/company/' . $request['enterprise_id'] . '/workers/' . $worker_id . '/', '');
        $doc_info = pathinfo($docname);
        $expire = $request['expire_date'];

        WorkerDocs::create([
            'documentName' => $doc_info['filename'],
            'documentNameOriginal' => $original,
            'document_extension' => $doc_info['extension'],
            'company_id' => $request['enterprise_id'],
            'worker_id' => $worker_id,
            'expires_at' => $expire,
            'fk_typeID' => $type['0'],
        ]);

        $data = new stdClass();
        $data->link = route('workers.show', $worker_id);
        $data->what = 'Ficheiro de Colaborador Adicionado';
        $to = 'diogo.adao@visual-thinking.pt';
        Mail::to($to)->send(new EmailSender($data));
        $user = auth()->user();
        if ($user->hasRole('manager')) {
            return redirect()
                ->route('workers.show', $worker_id)
                ->with('success', 'Utilizador Atualizado Com Sucesso');
        }
        return redirect()
            ->route('index')
            ->with('success', 'Documento Atualizado Com Sucesso');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return string
     */
    public function destroy($id)
    {
        $state = User::all();

        $state = $state->where('id', '=', $id)
            ->pluck('state')
            ->toArray();

        $user = User::find($id);

        if ($state[0] == '1') {

            $user->state = '0';

        } else {

            $user->state = '1';
        }
        $user->save();

        return redirect()
            ->route('workers.index')
            ->with('success', 'Cliente Atualizado Com Sucesso');
    }

    public function deleteworker(Request $request)
    {
        User::destroy($request['id']);
    }

    public function newDocType()
    {
        $user = auth()->user();

        return view('workers.DocType')->with('user', $user);
    }

    public function POST_type(Request $request)
    {

        $expires = FALSE;
        if ($request['expires'] == 'on') {
            $expires = TRUE;
        }
        WorkerDocsTypes::create([
            'typeName' => $request['name'],
            'expires' => $expires,
        ]);

        return redirect()
            ->route('workers.index')
            ->with('success', 'Tipo De Ficheiro Adicionado Com Sucesso');
    }
}