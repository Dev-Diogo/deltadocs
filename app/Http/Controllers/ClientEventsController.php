<?php

    namespace App\Http\Controllers;

    use App\Event;
    use App\EventDoc;
    use App\User;
    use App\WorkerDocs;
    use Illuminate\Http\Request;
    use Response;
    use Storage;

    class ClientEventsController extends Controller
    {
        /**
         * Display a listing of the resource.
         * @return \Illuminate\Http\Response
         */
        public function index()
        {
            $user = auth()->id();
            $events = Event::all()->where('clientID', '=', $user);
            return view('clientevents', compact('events'));
        }

        /**
         * Show the form for creating a new resource.
         * @return \Illuminate\Http\Response
         */
        public function create()
        {
            //
        }

        /**
         * Store a newly created resource in storage.
         *
         * @param \Illuminate\Http\Request $request
         *
         * @return \Illuminate\Http\Response
         */
        public function store(Request $request)
        {
            //
        }

        /**
         * Display the specified resource.
         *
         * @param int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function show($id)
        {
            $event = Event::all()->where('eventID', '=', $id)->where('clientID', '=', auth()->id())->count();

            if ($event > 0) {
                $workers = EventDoc::all()->where('eventID', '=', $id)->groupBy('workerID');
                $event = Event::find($id);
                $isclient = true;
                return view('client.event.show', compact('workers', 'event', 'isclient'));
            }
            return back();

        }

        /**
         * Show the form for editing the specified resource.
         *
         * @param int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function worker($id, $workerID)
        {
            $event = Event::all()->where('eventID', '=', $id)->where('clientID', '=', auth()->id())->count();
            if ($event > 0) {
                $docs = EventDoc::all()->where('eventID', '=', $id)->where('workerID', '=', $workerID)->count();
                if ($docs > 0) {
                    $docs = EventDoc::all()->where('eventID', '=', $id)->where('workerID', '=', $workerID);
                    $worker = User::find($workerID);
                    $isclient = true;
                    return view('client.event.docs.show', compact('docs', 'worker', 'isclient'));
                }
                return redirect()->route('index');
            }
            return back();
        }

        /**
         * Update the specified resource in storage.
         *
         * @param \Illuminate\Http\Request $request
         * @param int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function update(Request $request, $id)
        {
            //
        }

        /**
         * Remove the specified resource from storage.
         *
         * @param int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function destroy($id)
        {
            //
        }

        public function getDownload(Request $request)
        {
            $attributes = explode('.', $request['doc']);

            $doc = WorkerDocs::all()
                ->where('documentNameOriginal', '=', $attributes['0'])
                ->first();
            $request['doc'] = $doc['documentName'] . '.pdf';
            $file = Storage::download('/documents/company/' . $request['enterprise_id'] . '/workers/' . $request['id'] . '/' . $request['doc'],
                preg_replace('/[[:^print:]]/', '', $doc['documentNameOriginal']) . '.' . $doc['document_extension']);

            $strorage = Storage::disk('local')
                ->getAdapter()
                ->applyPathPrefix('/documents/company/' . $request['enterprise_id'] . '/workers/' . $request['id'] . '/' . $request['doc']);

            if ($doc['document_extension'] === 'pdf') {
                return Response::make(file_get_contents($strorage), 200, [
                    'Content-Type' => 'application/pdf',
                    'Content-Disposition' => 'inline; filename="' . preg_replace('/[[:^print:]]/', '',
                            $doc['documentNameOriginal'] . '.' . $doc['document_extension']) . '"',
                ]);
            } else {
                return $file;
            }

        }
    }
