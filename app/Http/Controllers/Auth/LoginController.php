<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Spatie\Permission\Traits\HasRoles;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;
    use HasRoles;

    /**
     * Create a new controller instance.
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    //protected $redirectTo = '/';

    /**
     * Where to redirect users after login.
     *
     * @param \Illuminate\Http\Request $request
     * @param $user
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */

    public function authenticated(Request $request, $user)
    {
        if (User::hasPermission('administrator', $user)) {
            return redirect('/admin');
        } else if (User::hasPermission('worker', $user)) {
            return redirect('/dashboard');
        } elseif (User::hasPermission('client', $user)) {
            return redirect('/clientevents');
        }
        return redirect('index');
    }
}
