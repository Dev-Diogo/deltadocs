<?php

namespace App\Http\Controllers;

use App\Enterprise;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Spatie\Permission\Models\Role;

/**
 * @method getEmailSubject()
 */
class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = User::all();

        return view('users.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (auth()->user()->hasPermissionTo('user-root')) {
            $roles = Role::whereNotIn('id', [1])
                ->pluck('name', 'name')
                ->all();
        } else {
            $roles = Role::whereNotIn('id', [1, 4])->pluck('name', 'name')
                ->all();
        }
        $enterprise = Enterprise::all();

        return view('users.create', compact('roles'))
            ->with('enterprise', $enterprise);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|same:confirm-password',
            'roles' => 'required',
        ]);

        $input = $request->all();
        $input['password'] = Hash::make($input['password']);

        $user = User::create($input);
        $user->assignRole($request->input('roles'));
        UserController::sendEmail($request->input('email'));

        return redirect()->route('users.index')
            ->with('success', 'Utilizador Criado Com Sucesso');
    }

    public function sendEmail($email_address)
    {
        $credentials = ['email' => $email_address];
        $response = Password::sendResetLink($credentials, function (Message $message) {
            $message->subject($this->getEmailSubject());
        });

        switch ($response) {
            case Password::RESET_LINK_SENT:
                return redirect()
                    ->back()
                    ->with('status', trans($response));

            case Password::INVALID_USER:
                return redirect()
                    ->back()
                    ->withErrors(['email' => trans($response)]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);

        return view('users.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if ($id == 1) {
            if (auth()->user()->can('user-root')) {

                $user = User::find($id);
                $enterprise = Enterprise::all();
                $roles = Role::whereNotIn('id', [1])
                    ->pluck('name', 'name')
                    ->all();

                $userRole = $user
                    ->roles
                    ->pluck('name', 'name')
                    ->all();

                return view('users.edit', compact('user', 'roles', 'userRole', 'enterprise'));
            } else {
                return redirect()
                    ->route('users.index');
            }
        } else {
            $user = User::find($id);

            $roles = Role::whereNotIn('id', [1])
                ->pluck('name', 'name')
                ->all();

            $userRole = $user
                ->roles
                ->pluck('name', 'name')
                ->all();

            $enterprise = Enterprise::all();

            return view('users.edit', compact('user', 'roles', 'userRole', 'enterprise'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email,' . $id,
            'password' => 'same:confirm-password',
            'roles' => 'required',
        ]);

        $input = $request->all();

        if (!empty($input['password'])) {

            $input['password'] = Hash::make($input['password']);

        } else {

            $input = array_except($input, ['password']);
        }

        $user = User::find($id);

        $user->update($input);

        DB::table('model_has_roles')
            ->where('model_id', $id)
            ->delete();

        $user->assignRole($request->input('roles'));

        return redirect()->route('users.index')
            ->with('success', 'Utilizador Atualizado Com Sucesso');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if ($id != '1') {
            User::find($id)->delete();

            return redirect()
                ->route('users.index')
                ->with('success', 'Utilizador Apagado Com Sucesso');

        } else {
            return redirect()
                ->route('users.index')
                ->with('error', 'Cannot deleted Root');
        }
    }

    public function PostEnterprise(Request $request)
    {
        Enterprise::create([
            'enterpriseName' => $request['enterpriseName'],
            'enterpriseAddress' => $request['enterpriseAddress'],
            'enterpriseNif' => $request['enterpriseNif'],
        ]);

        return redirect()
            ->route('users.index')
            ->with('success', 'Entidade Criada Com Sucesso');
    }

    public function PostEnterpriseEdit(Request $request)
    {

        $enterprise = Enterprise::find($request['enterpriseID']);

        $enterprise->enterpriseName = $request['enterpriseName'];

        $enterprise->save();

        return redirect()
            ->route('users.index')
            ->with('success', 'Entidade Criada Com Sucesso');
    }

    public function Enterprise()
    {
        return view('users.create_enterprise');
    }

    public function EnterpriseEdit()
    {
        $enterprise = Enterprise::all();

        return view('users.edit_enterprise')
            ->with('enterprise', $enterprise);
    }
}
