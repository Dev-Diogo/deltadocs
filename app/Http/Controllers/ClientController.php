<?php

namespace App\Http\Controllers;

use App\Enterprise;
use App\Event;
use App\EventDoc;
use App\User;
use App\WorkerDocs;
use Hash;
use Illuminate\Http\Request;
use Illuminate\Mail\Message;
use Illuminate\Support\Str;
use Password;
use Response;
use Storage;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clients = User::join('model_has_roles', 'model_has_roles.model_id', '=', 'users.id')
            ->Select('users.*', 'model_has_roles.role_id')
            ->where('model_has_roles.role_id', '=', '5')
            ->get();
        return view('client.index', compact('clients'));
    }

    /**
     * Delete Client from DataBase.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\Request
     */
    public function delete(Request $request)
    {
        User::destroy($request['id']);
        return Response(200);
    }

    /**
     * Delete Client from DataBase.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\Request
     */
    public function state(Request $request)
    {
        $user = User::find($request['id']);
        if ($user->state === '1' || $user->state === 1) {
            $user->state = 0;
            $user->save();
        } else {
            $user->state = 1;
            $user->save();
        }
        return $user->state;

    }

    /**
     * Show the form for creating a new resource.
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('client.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\Request
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
        ]);
        $input = $request->all();
        $input['password'] = Hash::make(str::random(18));

        $user = User::create($input);
        $user->assignRole('client');
        ClientController::sendEmail($request->input('email'));

        return redirect()->route('client.index')
            ->with('success', 'Utilizador Criado Com Sucesso');
    }

    public function sendEmail($email_address)
    {
        $credentials = ['email' => $email_address];
        $response = Password::sendResetLink($credentials, function (Message $message) {
            $message->subject($this->getEmailSubject());
        });

        switch ($response) {
            case Password::RESET_LINK_SENT:
                return redirect()
                    ->back()
                    ->with('status', trans($response));

            case Password::INVALID_USER:
                return redirect()
                    ->back()
                    ->withErrors(['email' => trans($response)]);
        }
    }

//    public function storeEvent(Request $request)
//    {
//        $event = new Event();
//        $event->eventName = $request['name'];
//        $event->eventDescription = $request['description'];
//        $event->eventStartDate = $request['start'];
//        $event->eventendDate = $request['end'];
//        $event->clientID = $request['id'];
//        $event->save();
//
//    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $events = Event::all()->where('clientID', '=', $id);
        $client = User::find($id);
        return view('client.show', compact('events', 'client'));
    }

    public function createEvent($id)
    {
        return view('client.event.create', compact('id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function storeEvent(Request $request)
    {
        $event = new Event();
        $event->eventName = $request['name'];
        $event->eventDescription = $request['description'];
        $event->eventStartDate = $request['start'];
        $event->eventendDate = $request['end'];
        $event->clientID = $request['id'];
        $event->final_client = $request['final_client'];
        $event->local = $request['local'];
        $event->save();
        return redirect()->route('client.show', $request['id']);
    }

    public function deleteEvent(Request $request)
    {
        $event = Event::find($request['id']);
        $event->delete();
        return response(200);

    }

    public function stateEvent(Request $request)
    {
        $event = Event::find($request['id']);
        if ($event->eventState === '1' || $event->eventState === 1) {
            $event->eventState = 0;
            $event->save();
        } else {
            $event->eventState = 1;
            $event->save();
        }
        return response(200);

    }

    public function showEvent($id)
    {
        $workers = EventDoc::all()->where('eventID', '=', $id)->groupBy('workerID');
        $event = Event::find($id);
        return view('client.event.show', compact('workers', 'event'));

    }

    public function addEventWorker($id)
    {
        $workers = User::join('model_has_roles', 'model_has_roles.model_id', '=', 'users.id')
            ->select('users.*', 'model_has_roles.role_id')
            ->where('model_has_roles.role_id', '=', '3')
            ->get();
        $company = Enterprise::all();
        return view('client.event.add', compact('workers', 'id','company'));
    }

    public function storeEventWorker (Request $request){
        $docs = $request['docs'];
        foreach ($docs as $doc){
            $eventDoc = new EventDoc();
            $eventDoc->EventDocName = $doc;
            $eventDoc->EventDocOriginalName = $doc;
            $eventDoc->EventID = $request['id'];
            $eventDoc->workerID = $request['worker'];
            $eventDoc->save();
        }
    return redirect()->route('client.show.event',$request['id']);
    }

    public function showWorkerDocs ($id, $wokerID){
        $docs = EventDoc::all()->where('eventID','=',$id)->where('workerID','=',$wokerID);
        $worker = User::find($wokerID);
        return view('client.event.docs.show',compact('docs','worker'));
    }

    public function getDownload(Request $request){
        $attributes = explode('.', $request['doc']);

        $doc = WorkerDocs::all()
            ->where('documentNameOriginal', '=', $attributes['0'])
            ->first();
        $request['doc'] = $doc['documentName'].'.pdf';
        $file = Storage::download('/documents/company/' . $request['enterprise_id'] . '/workers/' . $request['id'] . '/' . $request['doc'], preg_replace('/[[:^print:]]/', '', $doc['documentNameOriginal']) . '.' . $doc['document_extension']);

        $strorage = Storage::disk('local')
            ->getAdapter()
            ->applyPathPrefix('/documents/company/' . $request['enterprise_id'] . '/workers/' . $request['id'] . '/' . $request['doc']);

        if ($doc['document_extension'] === 'pdf') {
            return Response::make(file_get_contents($strorage), 200, [
                'Content-Type' => 'application/pdf',
                'Content-Disposition' => 'inline; filename="' . preg_replace('/[[:^print:]]/', '', $doc['documentNameOriginal'] . '.' . $doc['document_extension']) . '"',
            ]);
        } else {
            return $file;
        }

    }
}
