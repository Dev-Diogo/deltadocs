<?php

namespace App\Http\Controllers;

use App\Enterprise;
use App\User;
use App\WorkerDocs;
use App\WorkerDocsTypes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;

class WorkerDocsController extends Controller
{
    public function approve(Request $request)
    {

        $doc = WorkerDocs::all()
            ->where('documentName', '=', $request['doc'])
            ->first();

        $doc->valid = '1';

        $doc->save();

        return redirect()
            ->back()
            ->with('success', 'Aprovado Com Sucesso');
    }

    public function filter($id)
    {

        $filter = 1;

        $data = User::join('model_has_roles', 'model_has_roles.model_id', '=', 'users.id')
            ->select('users.*', 'model_has_roles.role_id')
            ->where('model_has_roles.role_id', '=', '3')
            ->where('users.fk_enterpriseID', '=', $id)
            ->get();

        $enterprise = Enterprise::find($id);

        $user = auth()->user();

        return view('workers.index', compact('data'))
            ->with('user', $user)
            ->with('filter', $filter)
            ->with('enterprise', $enterprise);
    }

    public function deny(Request $request)
    {

        $doc = WorkerDocs::all()
            ->where('documentName', '=', $request['doc'])
            ->first();

        $doc->description = $request['desc'];

        $doc->valid = '2';

        $doc->save();

        return redirect()
            ->back()
            ->with('success', 'Rejetitado com Sucesso');
    }

    public function document_download_admin(Request $request)
    {
        $attributes = explode('.', $request['doc']);

        $doc = WorkerDocs::all()
            ->where('documentName', '=', $attributes['0'])
            ->first();

        $file = Storage::download('/documents/company/' . $request['enterprise_id'] . '/workers/' . $request['id'] . '/' . $request['doc'], preg_replace('/[[:^print:]]/', '', $doc['documentNameOriginal']) . '.' . $doc['document_extension']);
        $strorage = Storage::disk('local')
            ->getAdapter()
            ->applyPathPrefix('/documents/company/' . $request['enterprise_id'] . '/workers/' . $request['id'] . '/' . $request['doc']);

        if ($doc['document_extension'] === 'pdf') {

            return Response::make(file_get_contents($strorage), 200, [
                'Content-Type' => 'application/pdf',
                'Content-Disposition' => 'inline; filename="' . preg_replace('/[[:^print:]]/', '', $doc['documentNameOriginal'] . '.' . $doc['document_extension']) . '"',
            ]);
        } else {
            return $file;
        }
    }

    public function document_download(Request $request)
    {
        $attributes = explode('.', $request['doc']);

        $doc = WorkerDocs::all()
            ->where('documentName', '=', $attributes['0'])
            ->first();

        $file = Storage::download('/documents/company/' . $request['enterprise_id'] . '/workers/' . $request['id'] . '/' . $request['doc'], preg_replace('/[[:^print:]]/', '', $doc['documentNameOriginal']) . '.' . $doc['document_extension']);

        $strorage = Storage::disk('local')
            ->getAdapter()
            ->applyPathPrefix('/documents/company/' . $request['enterprise_id'] . '/workers/' . $request['id'] . '/' . $request['doc']);

        if ($doc['document_extension'] === 'pdf') {
            return Response::make(file_get_contents($strorage), 200, [
                'Content-Type' => 'application/pdf',
                'Content-Disposition' => 'inline; filename="' . preg_replace('/[[:^print:]]/', '', $doc['documentNameOriginal'] . '.' . $doc['document_extension']) . '"',
            ]);
        } else {
            return $file;
        }
    }

    public function deleteType(Request $request)
    {
        WorkerDocsTypes::destroy($request['typename']);

        return redirect()
            ->route('workers.index')
            ->with('success', 'Tipo De Documento Apagado com Sucesso');
    }

    public function deletedoc(Request $request)
    {
        WorkerDocs::destroy($request['id']);
    }

    public function deleteview()
    {
        $types = WorkerDocsTypes::All();

        return view('workers.delete')
            ->with('types', $types);
    }

    public function  getWorkerDocs(Request $request){
        return WorkerDocs::all()->where('worker_id','=',$request['id']);
}
}
