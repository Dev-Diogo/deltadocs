<?php

namespace App\Http\Controllers;

use App\CompanyDocs;
use App\Enterprise;
use Illuminate\Http\Request;

class CompaniesController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = auth()
            ->user();

        $files = CompanyDocs::all()
            ->where('company_id', '!=', '1');

        return view('companies.index')
            ->with('docs', $files)
            ->with('user', $user);
    }

    /**
     * Display a listing of the resource.
     *
     * @param $id
     *
     * @return \Illuminate\Http\Response
     */
    public function details($id)
    {
        return $id;
    }

    /**
     * Show the form for creating a new resource.
     * @return void
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Request
     */
    public function detailsUpdate(Request $request)
    {
        $id = $request['id'];
        $enterprise = Enterprise::find($id);
        $enterprise->enterpriseName = $request['name'];
        $enterprise->enterpriseAddress = $request['address'];
        $enterprise->enterpriseNif = $request['nif'];
        $enterprise->save();
        return redirect()->route('admin.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     *
     * @return void
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return int
     */
    public function filter($id)
    {
        $user = auth()
            ->user();

        $enterprise = Enterprise::find($id);
        $files = CompanyDocs::all()
            ->where('company_id', '=', $id);

        return view('companies.index')
            ->with('docs', $files)
            ->with('user', $user)
            ->with('enterprise', $enterprise);
    }
}
