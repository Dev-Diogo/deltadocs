<?php

namespace App\Http\Controllers;

use App\Enterprise;
use App\User;
use App\WorkerDocs;
use Illuminate\Http\Request;

class OtherworkersController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $filter = 0;;
        $data = User::join('model_has_roles', 'model_has_roles.model_id', '=', 'users.id')
            ->select('users.*', 'model_has_roles.role_id')
            ->where('model_has_roles.role_id', '=', '3')
            ->where('users.fk_enterpriseID', '!=', '1')
            ->get();
        $user = auth()
            ->user();

        return view('otherworkers.index', compact('data'))
            ->with('user', $user)
            ->with('filter', $filter);
    }

    public function filter($id)
    {
        $filter = 1;
        $data = User::join('model_has_roles', 'model_has_roles.model_id', '=', 'users.id')
            ->select('users.*', 'model_has_roles.role_id')
            ->where('model_has_roles.role_id', '=', '3')
            ->where('users.fk_enterpriseID', '=', $id)
            ->get();
        $enterprise = Enterprise::find($id);
        $user = auth()->user();

        return view('otherworkers.index', compact('data'))
            ->with('user', $user)
            ->with('filter', $filter)
            ->with('enterprise', $enterprise);
    }

    /**
     * Show the form for creating a new resource.
     * @return void
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        $docs = WorkerDocs::all()
            ->where('worker_id', '=', $id);

        return view('otherworkers.show')
            ->with('user', $user)
            ->with('docs', $docs)
            ->with('id', $id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     *
     * @return void
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return void
     */
    public function destroy($id)
    {
        //
    }
}
