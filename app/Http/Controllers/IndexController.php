<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Support\Facades\Auth;

class IndexController extends Controller
{
    /**
     * Show the application dashboard.
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::check()) {
            if (User::hasPermission('administrator', auth()->user())) {
                return redirect('/admin');
            } elseif (User::hasPermission('worker', auth()->user())) {
                    return redirect('/dashboard');

            } elseif (User::hasPermission('client', auth()->user())) {
                return redirect('/clientevents');

            }
        } else {
            return view('welcome');
        }
    }
}
