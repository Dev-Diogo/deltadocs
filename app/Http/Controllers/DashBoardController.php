<?php

namespace App\Http\Controllers;

use App\Enterprise;
use App\Purchase;
use App\Spend;
use App\User;
use App\WorkerDocs;
use Illuminate\Http\Request;

class DashBoardController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = auth()->user()->id;
        $user = User::find($data);
//        if($user->hasRole('super-admin') or $user->hasRole('admin')){
//            redirect()->route('admin.index');
//        }
        if ($user->hasRole('manager')) {
            $company = Enterprise::all()->where('enterpriseID', '=', $user->enterprise->enterpriseID);
            return view('clientdashboard')
                ->with('user_info', $data)
                ->with('data', $company);

        } else {
            $id = auth()->id();
            $user = User::find($id);

            $docs = WorkerDocs::join('workerdocs_types', 'workerdocs_types.WorkertypeID', '=', 'workerdocs.fk_typeID')
                ->select('workerdocs.*', 'workerdocs_types.*')
                ->where('worker_id', '=', $id)
                ->orderBy('typeName', 'desc')
                ->get();

            return view('workers.show')
                ->with('user', $user)
                ->with('docs', $docs)
                ->with('id', $id);
        }

    }

    /**
     * Show the form for creating a new resource.
     * @return void
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     *
     * @return void
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return void
     */
    public function destroy($id)
    {
        //
    }
}
