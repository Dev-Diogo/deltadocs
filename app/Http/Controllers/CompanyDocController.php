<?php

namespace App\Http\Controllers;

use App\CompanyDocs;
use App\DocCompanyType;
use App\Enterprise;
use App\Mail\EmailSender;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use stdClass;

class CompanyDocController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return array
     */
    public function index()
    {
        $filter = 0;

        $user = auth()
            ->user();

        $files = CompanyDocs::all();

        return view('companydocs.index')
            ->with('docs', $files)
            ->with('user', $user)
            ->with('filter', $filter);
    }

    /**
     * @param $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function filter($id)
    {
        $filter = 1;

        $user = auth()
            ->user();

        $enterprise = Enterprise::find($id);

        $files = CompanyDocs::all()
            ->where('company_id', '=', $id);

        return view('companydocs.index')
            ->with('docs', $files)
            ->with('user', $user)
            ->with('enterprise', $enterprise)
            ->with('filter', $filter);
    }

    public function deleteview()
    {
        $types = DocCompanyType::All();

        return view('companydocs.delete')
            ->with('types', $types);
    }

    /**
     * Show the form for creating a new resource.
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = auth()
            ->user();

        return view('companydocs.create')
            ->with('user', $user);
    }

    /**
     * Show the form for creating a new resource.
     * @return \Illuminate\Http\Response
     */
    public function upload()
    {
        $user = auth()
            ->user();

        $enterprise = Enterprise::all();

        $types = DocCompanyType::all();

        return view('companydocs.upload')
            ->with('user', $user)
            ->with('types', $types)
            ->with('enterprise', $enterprise);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */

    public function POST_type(Request $request)
    {
        $expires = FALSE;

        if ($request['expires'] == 'on') {
            $expires = TRUE;
        }

        DocCompanyType::create(
            ['typeName' => $request['name'], 'expires' => $expires,]
        );

        return redirect()
            ->route('company.index')
            ->with('success', 'Tipo De Ficheiro Adicionado Com Sucesso');
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \Symfony\Component\HttpFoundation\StreamedResponse
     */
    public function POST_getFiles(Request $request)
    {
        $attributes = explode('.', $request['doc']);

        $doc = CompanyDocs::all()
            ->where('documentName', '=', $attributes['0'])
            ->first();

        $file = Storage::download('/documents/company/' . $request['id'] . '/' . $request['doc'], preg_replace('/[[:^print:]]/', '', $doc['documentNameOriginal']));

        $strorage = Storage::disk('local')
            ->getAdapter()
            ->applyPathPrefix('/documents/company/' . $request['id'] . '/' . $request['doc']);

        if ($attributes['1'] === 'pdf') {
            return Response::make(file_get_contents($strorage), 200, [
                'Content-Type' => 'application/pdf',
                'Content-Disposition' => 'inline; filename="' . preg_replace('/[[:^print:]]/', '', $doc['documentNameOriginal']) . '"',
            ]);
        } else {
            return $file;
        }
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function POST_upload(Request $request)
    {
        $this->validate($request, [
            'doc' => 'mimes:pdf',
            'id' => 'required',
        ]);

        $doc = $request->file('doc');

        $original = $doc->getClientOriginalName();

        $original = explode('.', $original);

        $originalName = $original['0'];

        $originalExtension = $original['1'];

        $type = explode('/', $request['typename']);

        $count = CompanyDocs::all()
            ->where('company_id', '=', $request['id'])
            ->where('fk_typeID', '=', $type['0'])
            ->count();

        if ($count > 0) {

            $file = CompanyDocs::all()
                ->where('company_id', '=', $request['id'])
                ->where('fk_typeID', '=', $type['0'])
                ->first();

            CompanyDocs::destroy($file['company_documentID']);

            Storage::delete('/documents/company/' . $request['id'] . '/' . $file['documentName'] . '.' . $file['document_extension']);
        }
        $hash = uniqid() . File::extension($originalName);

        $url = $doc->storePubliclyAS('/documents/company/' . $request['id'] . '', '' . $hash . '.' .
            $originalExtension);


        $expire = $request['expire_date'];

        CompanyDocs::create([
            'documentName' => $hash,
            'documentNameOriginal' => $originalName,
            'document_extension' => $originalExtension,
            'company_id' => $request['id'],
            'expires_at' => $expire,
            'fk_typeID' => $type['0'],
        ]);
        $data = new stdClass();
        $data->link = route('company.filter', ['id' => $request['id']]);
        $data->what = 'Ficheiro de Entidade Adicionado';
        $to = 'diogo.adao@visual-thinking.pt';
        Mail::to($to)->send(new EmailSender($data));

        return redirect()
            ->route('company.index')
            ->with('success', 'Ficheiro Adicionado');
    }

    public function approve(Request $request)
    {
        $doc = CompanyDocs::all()
            ->where('documentName', '=', $request['doc'])
            ->first();

        $doc->valid = '1';

        $doc->save();

        return redirect()
            ->back()
            ->with('success', 'Aprovado Com Sucesso');
    }

    public function deny(Request $request)
    {
        $doc = CompanyDocs::all()
            ->where('documentName', '=', $request['doc'])
            ->first();

        $doc->valid = '2';

        $doc->save();

        $doc->description = $request['desc'];

        $doc->save();

        return redirect()
            ->back()
            ->with('success', 'Rejeitado Com Sucesso');
    }

    public function deleteType(Request $request)
    {
        DocCompanyType::destroy($request['typename']);

        return redirect()
            ->route('company.index')
            ->with('success', 'Tipo De Documento Apagado com Sucesso');
    }


}
