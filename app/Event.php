<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $eventID
 * @property string $eventName
 * @property string $eventStartDate
 * @property string $eventEndDate
 * @property int $clientID
 * @property string $eventDescription
 * @property string $created_at
 * @property string $updated_at
 * @property User $user
 * @property Eventsdoc[] $eventsdocs
 */
class Event extends Model
{
    /**
     * The primary key for the model.
     * @var string
     */
    protected $primaryKey = 'eventID';

    /**
     * @var array
     */
    protected $fillable = [
        'eventName', 'eventStartDate', 'final_client', 'local', 'eventEndDate', 'clientID', 'eventDescription',
        'eventState', 'created_at', 'updated_at',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'clientID');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function eventsdocs()
    {
        return $this->hasMany('App\Eventsdoc', 'eventID', 'eventID');
    }
}
