<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @method static join(string $string, string $string1, string $string2, string $string3)
 * @method static create(array $array)
 */
class WorkerDocs extends Model
{
    protected $table = 'workerdocs';

    protected $primaryKey = 'worker_documentID';

    protected $fillable = [
        'documentName',
        'document_extension',
        'company_id',
        'expires_at',
        'fk_typeID',
        'documentNameOriginal',
        'valid',
        'worker_id',
        'description',
    ];

    public function enterprise()
    {
        return $this->belongsTo('App\Enterprise', 'company_id', 'enterpriseID');
    }

    public function types()
    {
        return $this->hasone('App\WorkerDocsTypes', 'WorkertypeID', 'fk_typeID');
    }

    public function worker()
    {
        return $this->belongsTo('App\user', 'id', 'worker_id');
    }
}
