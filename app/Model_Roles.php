<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Model_Roles extends Model
{
    protected $table = 'model_has_roles';

    protected $primaryKey = 'role_id';

    public function user()
    {
        return $this->belongsTo('App\User', 'model_id', 'id');
    }
}
