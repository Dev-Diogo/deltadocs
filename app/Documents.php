<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Documents extends Model
{
    protected $table = 'documents';

    protected $primaryKey = 'user_id';

    protected $fillable = [
        'cc',
        'bv',
        'user_id',
    ];

    public function user()
    {
        return $this->belongsto('App\User', 'user_id', 'id');
    }
}
