<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;
use App\Model_Roles;
use App\Enterprise;

/**
 * @method static find(int $id)
 * @method static join(string $Table, string $Key, string $Operator, string $Key)
 * @method static orderBy(string $string, string $string1)
 * @method static create(array $input)
 */
class User extends Authenticatable
{
    use Notifiable;
    use HasRoles;

    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'fk_enterpriseID',
        'nif',
    ];

    /**
     * The attributes that should be hidden for arrays.
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    public static function hasPermission($permission, User $user)
    {
        if ($user->hasPermissionTo($permission)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function worker_check()
    {
        return $this->hasMany('App\Model_Roles');
    }

    public function workerdocs()
    {
        return $this->hasMany('App\WorkerDocs');
    }

    public function enterprise()
    {
        return $this->belongsTo(Enterprise::class, 'fk_enterpriseID', 'enterpriseID');
    }

}
