<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @method static create(array $array)
 */
class DocCompanyType extends Model
{
    protected $table = 'companydocs_types';

    protected $primaryKey = 'typeID';

    protected $fillable = [
        'typeName',
        'expires',
    ];
}
