<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $eventDocID
 * @property string $EventDocOriginalName
 * @property string $EventDocName
 * @property string $eventEndDate
 * @property int $eventID
 * @property int $workerID
 * @property string $created_at
 * @property string $updated_at
 * @property User $user
 * @property Event $event
 */
class EventDoc extends Model
{
    /**
     * The table associated with the model.
     * @var string
     */
    protected $table = 'eventsDocs';

    /**
     * The primary key for the model.
     * @var string
     */
    protected $primaryKey = 'eventDocID';

    /**
     * @var array
     */
    protected $fillable = [
        'EventDocOriginalName', 'EventDocName', 'eventEndDate', 'eventID', 'workerID', 'created_at', 'updated_at',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'workerID');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function event()
    {
        return $this->belongsTo('App\Event', 'eventID', 'eventID');
    }
}
