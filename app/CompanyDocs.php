<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @method static create(array $array)
 */
class CompanyDocs extends Model
{
    protected $table = 'Companydocs';

    protected $primaryKey = 'company_documentID';

    protected $fillable = [
        'documentName',
        'document_extension',
        'company_id',
        'expires_at',
        'fk_typeID',
        'documentNameOriginal',
        'valid',
        'description',
    ];

    public function enterprise()
    {
        return $this->belongsTo('App\Enterprise', 'company_id', 'enterpriseID');
    }

    public function types()
    {
        return $this->hasone('App\DocCompanyType', 'typeID', 'fk_typeID');
    }
}
