<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class WorkerDocs extends Migration
{
    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        Schema::create('workerdocs', function (Blueprint $table) {
            $table->increments('worker_documentID');
            $table->string('documentName');
            $table->string('documentNameOriginal');
            $table->string('document_extension');
            $table->text('description')->nullable();
            $table->integer('valid')->default(0);
            $table->integer('company_id')->unsigned()->index();
            $table->integer('worker_id')->unsigned()->index();
            $table->integer('fk_typeID')->unsigned()->index();
            $table->date('expires_at')->nullable();
            $table->timestamps();
            $table->foreign('company_id')
                ->references('enterpriseID')
                ->on('enterprise')
                ->onDelete('cascade');
            $table->foreign('fk_typeID')
                ->references('WorkertypeID')
                ->on('workerdocs_types')
                ->onDelete('cascade');
            $table->foreign('worker_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('workerdocs');
    }
}
