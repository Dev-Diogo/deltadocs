<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class RemoveDescriptionField extends Migration
{
    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        Schema::table('Companydocs_types', function ($table) {
            $table->dropColumn('description');
        });
        Schema::table('workerdocs_types', function ($table) {
            $table->dropColumn('description');
        });
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        Schema::table('Companydocs_types', function ($table) {
            $table->text('description');
        });
        Schema::table('workerdocs_types', function ($table) {
            $table->text('description');
        });
    }
}
