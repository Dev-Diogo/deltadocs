<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class EnterpriseTable extends Migration
{
    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {

        Schema::create('enterprise', function (Blueprint $table) {
            $table->increments('enterpriseID');
            $table->string('enterpriseName');
            $table->string('enterpriseAddress');
            $table->string('enterpriseNif');
            $table->timestamps();
        });
        Schema::table('users', function (Blueprint $table) {
            $table->integer('fk_enterpriseID')->unsigned()->nullable();
            $table->foreign('fk_enterpriseID')
                ->references('enterpriseID')
                ->on('enterprise')
                ->onDelete('cascade')->change();
        });


        DB::table('enterprise')->insert([
            'enterpriseName' => "Deltamatic SA",
            'enterpriseAddress' => "Sede",
            'enterpriseNif' => "123456789",
        ]);
        DB::table('users')->insert([
            'name' => "Root",
            'email' => "root@dev.com",
            'password' => '$2y$10$CTzgGSjGr5/lSPirGCRRzOtSeWtLci5JTECZLyDdDXrjkwlmVzHhu',
            'fk_enterpriseID' => '1',
        ]);
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('enterprise');
    }
}
