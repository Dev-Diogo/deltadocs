<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class WorkerDocsTypes extends Migration
{
    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        Schema::create('workerdocs_types', function (Blueprint $table) {
            $table->increments('WorkertypeID');
            $table->string('typeName');
            $table->text('description');
            $table->boolean('expires')->default(FALSE);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('workerdocs_types');
    }
}
