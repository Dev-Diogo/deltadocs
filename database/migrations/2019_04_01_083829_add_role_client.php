<?php

use Illuminate\Database\Migrations\Migration;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class AddRoleClient extends Migration
{
    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        Permission::create(['name' => 'client']);
        $role = Role::create(['name' => 'client']);
        $role->givePermissionTo('client');
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        //
    }
}
