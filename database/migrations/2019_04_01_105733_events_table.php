<?php


use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class EventsTable extends Migration
{
    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->increments('eventID');
            $table->string('eventName');
            $table->dateTime('eventStartDate');
            $table->dateTime('eventEndDate');
            $table->integer('clientID')->unsigned();
            $table->longText('eventDescription');
            $table->timestamps();
            $table->foreign('clientID')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        //
    }
}
