<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class EventsDocsTable extends Migration
{
    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        Schema::create('eventsDocs', function (Blueprint $table) {
            $table->increments('eventDocID');
            $table->string('EventDocOriginalName');
            $table->dateTime('EventDocName');
            $table->integer('eventID')->unsigned();
            $table->integer('workerID')->unsigned();
            $table->timestamps();
            $table->foreign('eventID')
                ->references('eventID')
                ->on('events')
                ->onDelete('cascade');
            $table->foreign('workerID')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        //
    }
}
