<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class CreateRoles extends Migration
{
    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        app()['cache']->forget('spatie.permission.cache');
        Permission::create(['name' => 'user-root']);
        Permission::create(['name' => 'administrator']);
        Permission::create(['name' => 'user-list']);
        Permission::create(['name' => 'user-add']);
        Permission::create(['name' => 'user-delete']);
        Permission::create(['name' => 'user-edit']);
        Permission::create(['name' => 'user-show']);
        Permission::create(['name' => 'client-list']);
        Permission::create(['name' => 'client-add']);
        Permission::create(['name' => 'client-delete']);
        Permission::create(['name' => 'client-edit']);
        Permission::create(['name' => 'client-show']);
        Permission::create(['name' => 'role-list']);
        Permission::create(['name' => 'role-create']);
        Permission::create(['name' => 'role-edit']);
        Permission::create(['name' => 'role-show']);
        Permission::create(['name' => 'role-delete']);
        Permission::create(['name' => 'manager']);
        Permission::create(['name' => 'worker']);

        $role = Role::create(['name' => 'super-admin']);
        $role->givePermissionTo(Permission::all());
        $role = Role::create(['name' => 'admin']);
        $role->givePermissionTo(Permission::all());
        $role->revokePermissionTo('user-root');
        $role = Role::create(['name' => 'worker']);

        $role->givePermissionTo('worker');
        $role = Role::create(['name' => 'manager']);

        $role->givePermissionTo('worker');
        $role->givePermissionTo('manager');
        DB::table('model_has_roles')->insert([
            'role_id' => "1",
            'model_type' => "App\User",
            'model_id' => '1',
        ]);
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {

    }
}
