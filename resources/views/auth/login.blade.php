@extends('layouts.app')
@section('css')
	<link href="{{asset('css/login-custom.css')}}" rel="stylesheet" type="text/css"/>
@endsection
@section('content')
	<body class="account-pages">
	<div class="accountbg"
	     style="background: url({{asset('images/logo2.jpg')}}) no-repeat center center;background-size: cover; "></div>

	<div class="wrapper-page semi-transparent account-page-full">

		<div class="card full-transparent">
			<div class="card-block full-transparent">

				<div class="account-box full-transparent">

					<div class="card-box p-5 full-transparent">
						<h2 class="text-uppercase text-center pb-4">
							<a href="index.html" class="text-success">
								<span><img src="{{asset('images/tiny-logo.png')}}" alt="" height="75"></span>
							</a>
						</h2>

						<form method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
							@csrf
							<div class="form-group m-b-20 row">
								<div class="col-12">
									<label for="emailaddress">Email</label>
									<input id="email" type="email" placeholder=""
									       class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
									       name="email" value="{{ old('email') }}" required autofocus>

									@if ($errors->has('email'))
										<span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
									@endif
								</div>
							</div>

							<div class="form-group row m-b-20">
								<div class="col-12">
									<a href="page-recoverpw.html" class="text-muted pull-right">
										<small>Recuperar Palavra Passe</small>
									</a>
									<label for="password">Senha</label>
									<input id="password" type="password" placeholder=""
									       class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
									       name="password" required>

									@if ($errors->has('password'))
										<span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
									@endif
								</div>
							</div>

							<div class="form-group row text-center m-t-10">
								<div class="col-12">
									<button class="btn btn-block btn-custom waves-effect waves-light"
									        type="submit">Entrar
									</button>
								</div>
							</div>

						</form>


					</div>
				</div>

			</div>
		</div>

		<div class="m-t-40 text-center">
			<p class="account-copyright">2018 © Highdmin. - Coderthemes.com</p>
		</div>
	</div>
	</body>
@endsection
