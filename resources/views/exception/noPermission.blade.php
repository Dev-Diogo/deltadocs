<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- CSRF Token -->
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<title>{{ config('app.name', 'Laravel') }}</title>

	<!-- Scripts -->

	<!-- Fonts -->
	<link rel="dns-prefetch" href="https://fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">
	<!-- Styles -->
	<link href="{{asset('css/bootstrap.css')}}" rel="stylesheet" type="text/css"/>
	<link href="{{asset('css/metismenu.min.css')}}" rel="stylesheet" type="text/css"/>
	<link href="{{asset('css/style.css')}}" rel="stylesheet" type="text/css"/>
	<link href="{{asset('css/style_dark.css')}}" rel="stylesheet" type="text/css"/>
	<link href="{{asset('css/login-custom.css')}}" rel="stylesheet" type="text/css"/>
	<link href="{{asset('css/icons.css')}}" rel="stylesheet" type="text/css"/>
	<link href="{{asset('css/fontawsome.css')}}" rel="stylesheet" type="text/css"/>
	<!-- App js -->
</head>
<body>
<div id="app">
	<main>
		<body class="account-pages">
		<div class="accountbg"
		     style="background: url({{asset('images/logo2.jpg')}}) no-repeat center center;background-size: cover; "></div>

		<div class="wrapper-page semi-transparent account-page-full">

			<div class="card full-transparent">
				<div class="card-block full-transparent">

					<div class="account-box full-transparent">

						<div class="card-box p-5 full-transparent">
							<h2 class="text-uppercase text-center pb-4">
								<a href="index.html" class="text-success">
									<span><img src="assets/images/logo.png" alt="" height="26"></span>
								</a>
							</h2>

							<div class="text-center">
								<h1 class="text-error text-black">403</h1>
								<h4 class="text-uppercase  mt-3 text-black">Acess Denied</h4>
								<p class=" mt-3 text-black">Your action is not allowed Contact the IT Administrator</p>

								<a class="btn btn-md btn-block btn-custom waves-effect waves-light mt-3"
								   href="dashboard">
									Return Home</a>
							</div>
						</div>

					</div>
				</div>
			</div>
		</body>
	</main>
</div>
<script src="{{asset('js/jquery.min.js')}}"></script>
<script src="{{asset('js/fontawsome.js')}}"></script>
<script src="{{asset('js/modernizr.min.js')}}"></script>

<script src="{{asset('js/popper.min.js')}}"></script>
<script src="{{asset('js/bootstrap.js')}}"></script>
<script src="{{asset('js/metisMenu.min.js')}}"></script>
<script src="{{asset('js/waves.js')}}"></script>
<script src="{{asset('js/jquery.slimscroll.js')}}"></script>
@yield('script')
<script src="{{asset('js/jquery.core.js')}}"></script>
<script src="{{asset('js/jquery.app.js')}}"></script>
</body>
</html>
