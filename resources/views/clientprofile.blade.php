@extends('layouts.app')
@section('css')
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.css"/>
	<link rel="stylesheet" type="text/css"
	      href="https://cdn.datatables.net/responsive/2.2.2/css/responsive.bootstrap4.min.css"/>

@endsection
@section('main')
	<div class="row">
		<div class="col-lg-12 margin-tb">
			<div class="text-center">
				<h2>Activity</h2>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12 margin-tb">
			<div class="card-box">
				<div class="card-title">
					<h3>Purchases</h3>
				</div>
				<div class="card-body">
					<table id="key-table-1" class="table table-bordered">
						<thead>
						<th>
							Payment ID
						</th>

						<th>
							Start Date
						</th>
						<th>
							Expiration date
						</th>
						<th>
							Time Purchased (H/m)
						</th>
						<th>
							Purchase Date
						</th>
						</thead>
						<tbody>
						@foreach($paid as $payment)
							<tr>
								<td>
									{{$payment->minutes_paid_id}}
									@if($payment->expires_at < \Carbon\Carbon::today())
										<label class="badge badge-danger">Expired</label>
									@elseif($payment->active_at < \Carbon\Carbon::today())
										<label class="badge badge-success">Active</label>
									@else
										<label class="badge badge-info">Not Active</label>
									@endif
								</td>
								<td>
									{{$payment->active_at}}
								</td>
								<td>
									{{$payment->expires_at}}
								</td>
								<td>
									{{$payment->Number_of_hours_paid}}
								</td>
								<td>
									{{$payment->created_at}}
								</td>
							</tr>
						@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="col-lg-12 margin-tb">
			<div class="card-box">
				<div class="card-title">
					<h3>Tasks</h3>
				</div>
				<div class="card-body">
					<table id="key-table-2" class="table table-bordered">
						<thead>
						<th>
							Task ID
						</th>
						<th>
							Date
						</th>
						<th>
							Description
						</th>
						<th>
							Time Spent (H/m)
						</th>
						</thead>
						<tbody>
						@foreach($spend as $spent)
							<tr>
								<td>
									{{$spent->minutes_spent_id}}
								</td>
								<td>
									{{$spent->created_at}}
								</td>
								<td>
									{{$spent->description}}
								</td>
								<td>
									{{$spent->hours_spent}}
								</td>
							</tr>
						@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	@if ($message = Session::get('success'))
		<div class="alert alert-success">
			<p>{{ $message }}</p>
		</div>
	@endif
	@if ($message = Session::get('error'))
		<div class="alert alert-danger">
			<p>{{ $message }}</p>
		</div>
	@endif





@endsection
@section('script')

	<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.18/r-2.2.2/datatables.min.js"></script>
	<script type="text/javascript"
	        src="https://cdn.datatables.net/responsive/2.2.2/js/dataTables.responsive.min.js"></script>
	<script type="text/javascript"
	        src="https://cdn.datatables.net/responsive/2.2.2/js/responsive.bootstrap4.min.js"></script>

	<script>
        $(document).ready(function () {
            $('#key-table-1').DataTable({
                responsive: true, bInfo: false, bLengthChange: false,
                order: [0, 'desc'],

            });
        });
	</script>
	<script>
        $(document).ready(function () {
            $('#key-table-2').DataTable({
                responsive: true, bInfo: false, bLengthChange: false,
                order: [0, 'desc'],
            });
        });
	</script>
@endsection