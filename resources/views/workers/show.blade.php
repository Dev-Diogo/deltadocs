@extends('layouts.app')
@section('css')
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.css"/>
	<link rel="stylesheet" type="text/css"
	      href="https://cdn.datatables.net/responsive/2.2.2/css/responsive.bootstrap4.min.css"/>
	<link rel="stylesheet" type="text/css"
	      href="{{asset('css/tables-custom.css')}}"/>
@endsection
@section('main')
	<style>
		.border300 {
			border-radius: 300px;
		!important;
			padding: 0;
		!important;
			margin: 0 0 0;
		!important;

		}

		.fa25 {
			font-size: 25px;
		!important;
		}
	</style>
	<div class="row">
		<div class="col-lg-12 margin-tb">
			<div class="pull-left">
				<h2>{{$user->name}} Docs</h2>
			</div>
			<div class="pull-right pd5-all">
				<a title="Submeter" class="btn btn-success " href="{{ route('workers.document.edit',$id) }}"><i
							class="fas fa-file-upload fa-1x"></i></a>
			</div>

		</div>
	</div>

	@if ($message = Session::get('success'))
		<div class="alert alert-success">
			<p>{{ $message }}</p>
		</div>
	@endif
	@if ($message = Session::get('error'))
		<div class="alert alert-danger">
			<p>{{ $message }}</p>
		</div>
	@endif
	<div class="div-bg-gray ">
		<div class="card-body">
			<table id="key-table" style="width: 100%;" class="table table-striped table-hover dataTable border-table"
			">
			<thead>

			<th>DOCUMENTO</th>
			<th>ESTADO</th>
			<th>VALIDADE</th>
			@can('administrator')
				<th>AÇÃO</th>
			@endcan
			</thead>
			<tbody>
			@foreach($docs as $doc)
				<tr>
					<td>
						<div class="pd5-all">
							@can('administrator')
								<form method="post" action="{{route('workers.document.downloadadmin')}}">
									@elsecan('manager')
										<form method="post" action="{{route('workers.document.download')}}">
											@endcan
											@csrf
											<input type="hidden" id="doc" name="doc"
											       value="{{$doc->documentName}}.{{$doc->document_extension}}">
											<input type="hidden" id="id" name="id" value="{{$user->id}}">
											<input type="hidden" id="enterprise_id" name="enterprise_id"
											       value="{{$user->enterprise->enterpriseID}}">
											<input type="hidden" id="hash" name="hash"
											       value="@php echo(Illuminate\Support\Facades\Hash::make( \Carbon\Carbon::today() && $doc->documentName && $user->email && $user->enterprise->enterpriseName));@endphp">
											<button class="btn"
											        style="padding-top: 3px;padding-bottom: 3px;"><i
														class="far fa-file-pdf fa-1x"></i>&ensp;{{$doc->types->typeName}}

											</button>
										</form>
								</form>
					</td>
					<td>
						<div class="pd5-all">
							@php
								$valid= $doc->valid;
								$expires_at = $doc->expires_at;

							if($valid == 2) {
							echo('<label data-toggle="tooltip" title="'.$doc->description.'" class="badge badge-danger pd5-all text-16"><i class="fas fa-times-circle fa-1x"></i></label>');
							 }else if($expires_at < Carbon\Carbon::today('Europe/Lisbon') && $expires_at != NULL ){
						   echo('<label data-toggle="tooltip" title="Documento Expirou" class="badge badge-danger pd5-all text-16"><i class="fas fa-times-circle fa-1x"></i></label>');
							   }
							else if($valid == 0)
							{
							echo('<label class="badge badge-warning pd5-all text-16"><i class="fas fa-exclamation-circle fa-1x"></i></label>');
							   }else if($valid == 1 && $expires_at !== NULL && $expires_at>\Carbon\Carbon::today()
							   ->addMonth(6)) {
							echo('<label class="badge badge-success pd5-all text-16"><i class="fas fa-check fa-1x"></i></label>');
						   }else if ($valid ==1 && $expires_at === NUll ){
						   echo('<label class="badge badge-success pd5-all text-16"><i class="fas fa-check fa-1x"></i></label>');
						   }else{
						   echo('<label class="badge badge-warning pd5-all text-16"><i class="fas fa-check
						   fa-1x"></i></label>');
						   }

							@endphp
						</div>
					</td>
					<td>
						<div class="pd5-all">
							@php
								$expire= $doc->expires_at;

							iF($expire == NULL)
							{
							 echo('<label class="badge">N/A</label>');
							}else if($expire < Carbon\Carbon::today('Europe/Lisbon') && $expires_at != NULL){
							echo('<label class="badge text-16">'.$expire.'</label>');
							}else if($expire < Carbon\Carbon::today('Europe/Lisbon')->addMonth(1) && $expires_at != NULL){
							echo('<label class="badge text-16">'.$expire.'</label>');
							}
							else{
							echo('<label class="badge text-16">'.$expire.'</label>');
							}
							@endphp
						</div>
					</td>
					@can('administrator')
						<td>
							<div class="row">

								@if(Auth::user()->enterprise->enterpriseID ==1)
									<div class="pd5-all">
										<form method="post" action="{{route('worker.document.approve')}}">
											@csrf
											<input type="hidden" id="doc" name="doc"
											       value="{{$doc->documentName}}">
											<button class="btn btn-success"
											        style="padding-top: 3px;padding-bottom: 3px;"><i
														class="far fa-thumbs-up"></i>
											</button>
										</form>
									</div>
									<div class="pd5-all">
										<form id="deny{{$doc->documentName}}.{{$doc->document_extension}}"
										      method="post"
										      action="{{route('worker.document.deny')}}">
											@csrf
											<input type="hidden" id="doc" name="doc"
											       value="{{$doc->documentName}}">
										</form>
										<button onclick="deny('deny{{$doc->documentName}}.{{$doc->document_extension}}');"
										        class="btn btn-danger"
										        style="padding-top: 3px;padding-bottom: 3px;"><i
													class="far fa-thumbs-down"></i>
										</button>

									</div>
									<div class="pd5-all">
										<button onclick="deleteDoc('{{$doc->worker_documentID}}');"
										        class="btn btn-danger"
										        style="padding-top: 3px;padding-bottom: 3px;"><i
													class="far fa-thumbs-down"></i>
										</button>
									</div>
									<div class="pd5-all">
										<input type="hidden"
										       id="reason_deny{{$doc->documentName}}.{{$doc->document_extension}}"
										       placeholder="Razão de Rejitação de Documento"
										       form="deny{{$doc->documentName}}.{{$doc->document_extension}}"
										       class="form-control" type="text" id="desc" name="desc"
										/>
									</div>
								@endif
							</div>
						</td>
					@endcan
				</tr>
			@endforeach
			</tbody>
			</table>

			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12">
					<div class="pull-left">
						<h4>Legenda</h4>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-2">
					<div>
						<strong>
							<a class="border300 btn btn-success"><i class="fa25 fas fa-check fa-1x"></i></a>&nbsp;Válido
						</strong>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-2">
					<div>
						<strong>
							<a class="border300 btn btn-warning"><i class="fa25 fas fa-check fa-1x"></i></a>&nbsp;
							Validade Reduzida
						</strong>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-2">
					<div>
						<strong>
							<a class="border300 btn btn-warning"><i class="fa25 fas
							fa-exclamation-circle"></i></a>&nbsp;Pendente
						</strong>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-2">
					<div>
						<strong>
							<a class="border300 btn btn-danger"><i class="fa25 fas fa-times-circle"></i></a>&nbsp;Inválido
						</strong>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-2">
					<div>
						<strong>
							<a class="border300 btn btn-info"><i class="fa25 fas fa-plus-circle"></i></a>&nbsp;Vazio
						</strong>
					</div>
				</div>
			</div>
		</div>
	</div>


@endsection
@section('script')

	<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.18/r-2.2.2/datatables.min.js"></script>
	<script type="text/javascript"
	        src="https://cdn.datatables.net/responsive/2.2.2/js/dataTables.responsive.min.js"></script>
	<script type="text/javascript"
	        src="https://cdn.datatables.net/responsive/2.2.2/js/responsive.bootstrap4.min.js"></script>
	<script type="text/javascript"
	        src="{{asset('js/sweetalert2.all.min.js')}}"></script>

	<script>
        $(document).ready(function () {
            $('#key-table').DataTable({
                responsive: true, bInfo: false, bLengthChange: false,
                stateSave: true,
                "language": {
                    "sEmptyTable": "Nenhum registo encontrado",
                    "sProcessing": "A processar...",
                    "sLengthMenu": "Mostrar _MENU_ registos",
                    "sZeroRecords": "Não foram encontrados resultados",
                    "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registos",
                    "sInfoEmpty": "Mostrando de 0 até 0 de 0 registos",
                    "sInfoFiltered": "(filtrado de _MAX_ registos no total)",
                    "sInfoPostFix": "",
                    "sSearch": "Procurar:",
                    "sUrl": "",
                    "oPaginate": {
                        "sFirst": "Primeiro",
                        "sPrevious": "Anterior",
                        "sNext": "Seguinte",
                        "sLast": "Último"
                    },
                    "oAria": {
                        "sSortAscending": ": Ordenar colunas de forma ascendente",
                        "sSortDescending": ": Ordenar colunas de forma descendente"
                    }
                }
            });
        });
	</script>
	<script>
        $(document).ready(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });

        function deny(id) {
            (async function getText() {
                const {value: text} = await swal({
                    input: 'textarea',
                    title: 'Motivo',
                    inputPlaceholder: 'Motivo de Rejeição',
                    showCancelButton: true
                });

                if (text) {

                    var element = document.getElementById('reason_' + id);
                    element.value = text;

                    document.getElementById(id).submit();
                }

            })()
        }

        function deleteDoc(id) {
            swal({
                title: 'Confirmação?',
                text: "Esta Ação Sera Permanente",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                    $.post("{{route('worker.document.delete')}}", {id: id, _token: "{{csrf_token()}}"});
                    swal(
                        'Deleted!',
                        'Your file has been deleted.',
                        'success',
                    ).then(() => {
                        location.reload(true);
                    })
                }
            })
        }
	</script>

@endsection