@extends('layouts.app')
@section('css')
	<link rel="stylesheet" type="text/css"
	      href=" https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css"/>
	<link rel="stylesheet" type="text/css"
	      href="{{asset('custom/jquery-ui-1.12.1.custom/jquery-ui.css')}}"/>
@endsection
@section('main')
	<div class="row">
		<div class="col-lg-12 margin-tb">
			<div class="pull-left">
				<h2>Carregar Documento de Colaborador</h2>
			</div>
			<div class="pull-right ">
				<a class="btn btn-primary" href="{{ route('workers.index') }}"><i class="fas fa-arrow-circle-left"></i></a>
			</div>
		</div>
	</div>
	@if (count($errors) > 0)
		<div class="alert alert-danger">
			<strong>Whoops!</strong> There were some problems with your input.<br><br>
			<ul>
				@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
	@endif
	<div class="container-fluid">
		<form id="form" name="form" action="{{route('workers.document.updatedoc')}}" method="post"
		      enctype="multipart/form-data">
			@csrf
			<div class="row">
				<input type="hidden" name="enterprise_id" value="{{$user->enterprise->enterpriseID}}">
				<input type="hidden" name="worker_id" value="{{$id}}">
				<div class="col-xs-12 col-sm-12 col-md-12">

					<div class="form-group">
						<strong>Tipo de Documento:</strong>
						<select form="form" name="typename" style="width: 200px" id="typeid">
							<option></option>
							@foreach($types as $type)
								<option value="{{$type->WorkertypeID}}/{{$type->expires}}">{{$type->typeName}}
								</option>
							@endforeach
						</select>
					</div>
				</div>
				<div style="display: none;" id="expire_date" class="col-xs-12 col-sm-12 col-md-12">
					<div class="form-group">
						<strong>Data de Expiração :</strong>
						<input autocomplete="off" type="text" class="form-control" name="expire_date" id="datepicker">
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-12">
					<div class="form-group">
						<strong>Documento:</strong>
						<div class="form-group">
							<input type="file" name="doc" id="doc">
						</div>
					</div>
				</div>

			</div>
			<div class="col-xs-12 col-sm-12 col-md-12 text-center">
				<button id="submit" type="submit" class="btn btn-primary">Submeter</button>
			</div>
	</div>
	</form>
	</div>


@endsection
@section('script')
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.full.js"></script>
	<script src="{{asset('custom/jquery-ui-1.12.1.custom/jquery-ui.min.js')}}"></script>
	<script>
        $("#typeid").select2({
            placeholder: "Escolha o Tipo de Documento",
            allowClear: false
        });

        $('#doc').change(function () {
            let file = $('#doc')[0].files[0].name;
            $('#doclabel').text(function (index) {
                return file;
            })
        });
        $('#typeid').on('select2:select', function (e) {
                let value = $('#typeid').val();
                let atributes = value.split('/');
                console.log(atributes);
                let x = document.getElementById("expire_date");
                if (atributes['1'] === "1") {
                    x.style.display = "block";
                } else {
                    x.style.display = "none";
                    $("#datepicker").datepicker('setDate', null);
                }
            }
        );
        $(function () {
            $("#datepicker").datepicker({dateFormat: 'yy-mm-dd'});
        });

	</script>
@endsection