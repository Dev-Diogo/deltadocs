@extends('layouts.app')
@section('css')
	<link rel="stylesheet" type="text/css"
	      href=" https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css"/>
	<link rel="stylesheet" type="text/css"
	      href="{{asset('custom/jquery-ui-1.12.1.custom/jquery-ui.css')}}"/>
@endsection
@section('main')
	<div class="row">
		<div class="col-lg-12 margin-tb">
			<div class="pull-left">
				<h2>Remover Tipo de Documento</h2>
			</div>
			<div class="pull-right ">
				<a class="btn btn-primary" href="{{ route('workers.index') }}"> Voltar</a>
			</div>
		</div>
	</div>
	@if (count($errors) > 0)
		<div class="alert alert-danger">
			<strong>Whoops!</strong> There were some problems with your input.<br><br>
			<ul>
				@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
	@endif
	<div class="container-fluid">
		<form id="form" name="form" action="{{route('workers.delete')}}" method="post"
		      enctype="multipart/form-data">
			@csrf
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12">
					<div class="form-group">
						<strong>Tipo De Documento:</strong>
						<select form="form" name="typename" style="width: 200px" id="typeid">
							<option></option>
							@foreach($types as $type)
								<option value="{{$type->WorkertypeID}}">{{$type->typeName}}
								</option>
							@endforeach
						</select>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-12 text-center">
				<button id="submit" type="submit" class="btn btn-primary">Remover</button>
			</div>
	</div>
	</form>
	</div>


@endsection
@section('script')
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.full.js"></script>
	<script src="{{asset('custom/jquery-ui-1.12.1.custom/jquery-ui.min.js')}}"></script>
	<script>
        $("#typeid").select2({
            placeholder: "Select a Document Type",
            allowClear: false
        });
	</script>
@endsection