@extends('layouts.app')
@section('css')
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.css"/>
	<link rel="stylesheet" type="text/css"
	      href="https://cdn.datatables.net/responsive/2.2.2/css/responsive.bootstrap4.min.css"/>
	<link rel="stylesheet" type="text/css"
	      href="{{asset('css/tables-custom.css')}}"/>
@endsection
@section('main')
	<style>
		.border300 {
			border-radius: 300px;
		!important;
			padding: 0;
		!important;
			margin: 0 0 0;
		!important;

		}

		.fa25 {
			font-size: 25px;
		!important;
		}
	</style>
	<div class="row">
		<div class="col-lg-12 margin-tb">
			<div class="pull-left">
				@if($filter == 1)
					<h2>{{$enterprise->enterpriseName}}/Colaboradores</h2>
				@else
					<h2>Colaboradores/Prestadores de Serviços</h2>
				@endif
			</div>
			@if($filter == 1)
				<div class="col-lg-12 margin-tb">
					<div class="pull-right pd5-all ">
						<a class="btn btn-primary" href="{{ route('admin.index') }}"> Voltar</a>
					</div>
				</div>
			@endif
			<div class="pull-right pd5-all">
				<a title="Novo Colaborador" class="btn btn-success" href="{{ route('workers.create') }}"><i
							class="fas fa-user-plus fa-1x"></i></a>
			</div>
			@can('manager')
				@if(Auth::user()->enterprise->enterpriseID ==1)
					<div class="pull-right pd5-all">
						<a title="Adicionar" class="btn btn-success" href="{{ route('workers.document.type') }}"><i
									class="far fa-plus-square fa-1x"></i></a>
					</div>
					<div class="pull-right pd5-all">
						<a title="Remover" class="btn btn-success" href="{{ route('workers.deleteview') }}"><i
									class="far fa-minus-square fa-1x"></i></a>
					</div>
					@if($filter == 0)
						<div class="col-lg-12 margin-tb">
							<div class="pull-right pd5-all">
								<a id="all" name="all" value="{{$user->enterprise->enterpriseName}}"
								   class="btn btn-success">Todos</a>
							</div>
						</div>
						<div class="col-lg-12 margin-tb">
							<div class="pull-right pd5-all">
								<a id="mine" name="mine" value="{{$user->enterprise->enterpriseName}}"
								   class="btn btn-success">Interno</a>
							</div>
						</div>
					@endif
				@endif
			@endcan

		</div>
	</div>

	@if ($message = Session::get('success'))
		<div class="alert alert-success">
			<p>{{ $message }}</p>
		</div>
	@endif
	@if ($message = Session::get('error'))
		<div class="alert alert-danger">
			<p>{{ $message }}</p>
		</div>
	@endif
	<div class="div-bg-gray ">
		<div class="card-body">
			<table id="key-table" style="width: 100%;"
			       class="table table-striped table-hover dataTable border-table border-table">
				<thead>
				@if($filter != 1)
					<th>ENTIDADE</th>
				@endif
				<th>COLABORADOR</th>
				<th>ESTADO DOS DOCUMENTOS</th>
				<th>AÇÃO</th>
				</thead>
				<tbody>
				@foreach($data as $worker)
					@if($user->enterprise->enterpriseID == $worker->enterprise->enterpriseID or $user->enterprise->enterpriseID == 1)

						<tr>
							@if($filter != 1)
								<td>{{$worker->enterprise->enterpriseName}}</td>
							@endif
							<td>
								<button style="background-color: transparent; " class="btn"
								        onclick="showModal({{$worker->id}})">{{$worker->name}}</button>

								<div id="{{$worker->id}}" name="{{$worker->id}}"
								     class="modal fade bd-example-modal-lg"
								     tabindex="-1"
								     role="dialog"
								     aria-labelledby="myLargeModalLabel" aria-hidden="true">
									<div class="modal-dialog modal-lg">
										<div class="modal-content">
											<div class="modal-header text-black-50">{{$worker->name}}</div>
											<div class="card">

												<div class="card-body">
													<div class="row">
														<form method="POST"
														      id="{{$worker->id}}{{$worker->name}}"
														      name="" id="{{$worker->id}}{{$worker->name}}
																"
														      class="mgl-25 mgr-25 width-100">
															@csrf
															<div class="form-group">
																<label for="id">Identificador:</label>
																<input type="id" name="id" class="form-control" id="id"
																       placeholder="{{$worker->id}}"
																       value="{{$worker->id}}"
																       disabled>
															</div>
															<div class="form-group">
																<label for="name">Nome:</label>
																<input type="text" name="name" class="form-control"
																       id="name"
																       placeholder="{{$worker->name}}"
																       value="{{$worker->name}}"
																       disabled>
															</div>
															<div class="form-group">
																<label for="name">Entidade:</label>
																<input type="text" name="name" class="form-control"
																       id="name"
																       placeholder="{{$worker->enterprise->enterpriseName}}"
																       value="{{$worker->enterprise->enterpriseName}}"
																       disabled>
															</div>
															<div class="form-group">
																<label for="name">Email:</label>
																<input type="text" name="name" class="form-control"
																       id="name"
																       placeholder="{{$worker->email}}"
																       value="{{$worker->email}}"
																       disabled>
															</div>
															<div class="form-group">
																<label for="name">NIF:</label>
																<input type="text" name="name" class="form-control"
																       id="name"
																       placeholder="{{$worker->nif}}"
																       value="{{$worker->nif}}"
																       disabled>
															</div>
														</form>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>

							</td>

							<td>
								@php
									$docs = \App\WorkerDocs::all()->where('company_id','=',$worker->enterprise->enterpriseID);
									$count_pend = $docs->where('valid','=','0')->where('worker_id','=',$worker->id)->count();
									$count_den = $docs->where('valid','=','2')->where('worker_id','=',$worker->id)->count();
									$count_app = $docs->where('valid','=','1')->where('worker_id','=',$worker->id)->count();
									$count_exp = $docs->where('expires_at','<',\Carbon\Carbon::today('Europe/Lisbon')
									)->where('valid','!=','2')->where('worker_id','=',$worker->id)->count();
									$count_exp_6month = $docs->where('valid','=','1')->where('expires_at','<',
									\Carbon\Carbon::today()->addMonth(6))->count();
								if($count_den >0 or $count_exp > 0){
								if($worker->state == '1'){
											 echo('<a class="border300 btn btn-danger "
										   href="'.route('workers.show',$worker->id).'"><i class="fa25 fas
										   fa-times-circle fa-1x"></i></a>');
								   }
									else{
									 echo('<a class="border300 btn btn-danger disabled "
										   href="'.route('workers.show',$worker->id).'"><i class="fa25 fas
										   fa-times-circle fa-1x"></i></a>');
									}

								  //echo('<label class="badge badge-warning pd5-all text-16"><i class="fas fa-exclamation-triangle fa-2x"></i></label>');
								   }else if($count_pend >0){
								   if($worker->state == '1'){
											 echo('<a class="border300 btn btn-warning"
										   href="'.route('workers.show',$worker->id).'"><i class="fa25 fas
										   fa-exclamation-circle fa-1x"></i></a>');
								   }
									else{
									 echo('<a class="border300 btn btn-warning  disabled"
										   href="'.route('workers.edit',$worker->id).'"><i class="fa25 fas
										   fa-plus-circle"></i></a>');
									}

								  //echo('<label class="badge badge-info pd5-all text-16"><i class="fas fa-exclamation-circle fa-1x"></i></label>');
								  }
								else if($count_app){
								if($worker->state == '1'){
										if($count_exp_6month >0){
										echo('<a class="border300 btn btn-warning"
										   href="'.route('workers.show',$worker->id).'"><i class="fa25 fas fa-check
										   fa-1x"></i></a>');
										}else{
										echo('<a class="border300 btn btn-success"
										   href="'.route('workers.show',$worker->id).'"><i class="fa25 fas fa-check
										   fa-1x"></i></a>');
										}

								   }
									else{
									 echo('<a class="border300 btn btn-success disabled"
										   href="'.route('workers.edit',$worker->id).'"><i class="fa25 fas fa-check
										   fa-1x"></i></a>');
									}

								//echo('<label class="badge badge-success pd5-all text-16"><i class="fas fa-check fa-2x"></i></label>');
								  }else{
								  if($worker->state == '1'){
											 echo('<a class="border300 btn btn-info"
										   href="'.route('workers.show',$worker->id).'"><i class="fa25 fas
										   fa-plus-circle"></i></a>');
								   }
									else{
									 echo('<a class="border300 btn btn-warning  disabled"
										   href="'.route('workers.edit',$worker->id).'"><i class="fa25 fas
										   fa-file-upload fa-1x"></i></a>');
									}

								  //echo('<label class="badge badge-warning pd5-all text-16"><i class="fas fa-file-upload fa-2x"></i></label>');
								  }

								@endphp
							</td>
							<td>
								<div class="row">
									@can('manager')
										<div class="pd5-all">
											<a title="Editar!" class="btn  btn-primary" style="padding-top: 3px;
										padding-bottom: 3px;"
											   href="{{ route('workers.edit',$worker->id) }}"><i
														class="far fa-edit"></i></a>
										</div>
									@endcan
									@can('manager')
										<div class="pd5-all">
											<button title="Apagar!" onclick="deleteCob('{{$worker->id}}');"
											        class="btn btn-danger"
											        style="padding-top: 3px;padding-bottom: 3px;"><i
														class="far fa-thumbs-down"></i>
											</button>
										</div>
									@endcan
								</div>
							</td>

						</tr>
					@endif
				@endforeach

				</tbody>
			</table>

			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12">
					<div class="pull-left">
						<h4>Legenda</h4>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-2">
					<div>
						<strong>
							<a class="border300 btn btn-success"><i class="fa25 fas fa-check fa-1x"></i></a>&nbsp;Válido
						</strong>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-2">
					<div>
						<strong>
							<a class="border300 btn btn-warning"><i class="fa25 fas fa-check fa-1x"></i></a>&nbsp;
							Validade Reduzida
						</strong>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-2">
					<div>
						<strong>
							<a class="border300 btn btn-warning"><i class="fa25 fas
							fa-exclamation-circle"></i></a>&nbsp;Pendente
						</strong>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-2">
					<div>
						<strong>
							<a class="border300 btn btn-danger"><i class="fa25 fas fa-times-circle"></i></a>&nbsp;Inválido
						</strong>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-2">
					<div>
						<strong>
							<a class="border300 btn btn-info"><i class="fa25 fas fa-plus-circle"></i></a>&nbsp;Vazio
						</strong>
					</div>
				</div>
			</div>
		</div>
	</div>




@endsection
@section('script')

	<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.18/r-2.2.2/datatables.min.js"></script>
	<script type="text/javascript"
	        src="https://cdn.datatables.net/responsive/2.2.2/js/dataTables.responsive.min.js"></script>
	<script type="text/javascript"
	        src="https://cdn.datatables.net/responsive/2.2.2/js/responsive.bootstrap4.min.js"></script>
	<script type="text/javascript"
	        src="{{asset('js/sweetalert2.all.min.js')}}"></script>
	<script>
        $(document).ready(function () {
            var table = $('#key-table').DataTable({
                responsive: true, bInfo: false, bLengthChange: false,
                order: [0, 'desc'],
                stateSave: true,
                "language": {
                    "sEmptyTable": "Nenhum registo encontrado",
                    "sProcessing": "A processar...",
                    "sLengthMenu": "Mostrar _MENU_ registos",
                    "sZeroRecords": "Não foram encontrados resultados",
                    "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registos",
                    "sInfoEmpty": "Mostrando de 0 até 0 de 0 registos",
                    "sInfoFiltered": "(filtrado de _MAX_ registos no total)",
                    "sInfoPostFix": "",
                    "sSearch": "Procurar:",
                    "sUrl": "",
                    "oPaginate": {
                        "sFirst": "Primeiro",
                        "sPrevious": "Anterior",
                        "sNext": "Seguinte",
                        "sLast": "Último"
                    },
                    "oAria": {
                        "sSortAscending": ": Ordenar colunas de forma ascendente",
                        "sSortDescending": ": Ordenar colunas de forma descendente"
                    }
                }
            });
            $("#all").click(function () {
                table.search("").draw();
            });
            $("#mine").click(function () {
                table.search("{{$user->enterprise->enterpriseName}}").draw();
            });
        });

        function deleteCob(id) {
            swal({
                title: 'Confirmação?',
                text: "Tem a certeza de que pretende eliminar este colaborador?",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Sim, Apagar Colaborador'
            }).then((result) => {
                if (result.value) {
                    $.post("{{route('workers.delete.permanent')}}", {id: id, _token: "{{csrf_token()}}"});
                    swal(
                        'Deleted!',
                        'Your file has been deleted.',
                        'success',
                    ).then(() => {
                        location.reload(true);
                    })
                }
            })
        }

        function showModal(id) {
            const inputs = document.getElementsByTagName('input');
            for (let i = 0; i < inputs.length; i++) {
                inputs[i].disabled = true;
            }
            $('#' + id).modal('show');

        }
	</script>
@endsection