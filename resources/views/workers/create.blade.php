@extends('layouts.app')
@section('css')
	<link rel="stylesheet" type="text/css"
	      href=" https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css"/>
	<link rel="stylesheet" type="text/css"
	      href="{{asset('custom/jquery-ui-1.12.1.custom/jquery-ui.css')}}"/>
@endsection
@section('main')
	<div class="row">
		<div class="col-lg-12 margin-tb">
			<div class="pull-left">
				<h2>Novo Colaborador</h2>
			</div>
			<div class="pull-right">
				<a class="btn btn-primary" href="{{ route('workers.index') }}"> Voltar</a>
			</div>
		</div>
	</div>

	@if (count($errors) > 0)
		<div class="alert alert-danger">
			<strong>Whoops!</strong> There were some problems with your input.<br><br>
			<ul>
				@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
	@endif
	{!! Form::open(array('id'=>'form','route' => 'workers.store','method'=>'POST')) !!}
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12">
			<div class="form-group">
				<strong>Nome:</strong>
				{!! Form::text('name', null, array('placeholder' => 'Name','class' => 'form-control')) !!}
			</div>
		</div>
		@can('administrator')
			<div class="col-xs-12 col-sm-12 col-md-12">
				<div class="form-group">
					<strong>Entidade:</strong>
					<select form="form" name="fk_enterpriseID" style="width: 200px" id="enterprise">
						<option></option>
						@foreach($enterprise as $c)
							@if($c->enterpriseID == $user->enterprise->enterpriseID )
								<option selected="selected" value="{{$c->enterpriseID}}">{{$c->enterpriseName}}
								</option>
							@else
								<option value="{{$c->enterpriseID}}">{{$c->enterpriseName}}
								</option>
							@endif
						@endforeach
					</select>
				</div>
			</div>
		@else
			<input type="hidden" name="fk_enterpriseID" value="{{$user->enterprise->enterpriseID}}">
		@endif
		<div class="col-xs-12 col-sm-12 col-md-12">
			<div class="form-group">
				<strong>Email:</strong>
				{!! Form::text('email', null, array('placeholder' => 'Email','class' => 'form-control')) !!}
			</div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-12 text-center">
			<button type="button" onclick="makeid()" class="btn btn-primary">Gerar Password</button>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-12">
			<div class="form-group">
				<strong>Password:</strong>
				{!! Form::password('password', array('id'=> 'password','placeholder' => 'Password','class' => 'form-control')) !!}
			</div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-12">
			<div class="form-group">
				<strong>Confirma Password:</strong>
				{!! Form::password('confirm-password', array('id'=> 'password_conf','placeholder' => 'Confirma Password','class' => 'form-control')) !!}
			</div>
		</div>

		<div class="col-xs-12 col-sm-12 col-md-12 text-center">
			<button type="submit" class="btn btn-primary">Submeter</button>
		</div>
	</div>
	{!! Form::close() !!}

@endsection

@section('script')
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.full.js"></script>
	<script src="{{asset('custom/jquery-ui-1.12.1.custom/jquery-ui.min.js')}}"></script>
	<script>
        function makeid() {
            var text = "";
            var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

            for (var i = 0; i < 7; i++)
                text += possible.charAt(Math.floor(Math.random() * possible.length));

            document.getElementById('password').value = text;
            document.getElementById('password_conf').value = text;
        }


	</script>

	<script>
        $("#enterprise").select2({
            placeholder: "Escolha a Entidade",
            allowClear: false
        });
	</script>
@endsection