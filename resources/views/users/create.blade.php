@extends('layouts.app')
@section('css')
	<link rel="stylesheet" type="text/css"
	      href=" https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css"/>
	<link rel="stylesheet" type="text/css"
	      href="{{asset('custom/jquery-ui-1.12.1.custom/jquery-ui.css')}}"/>
@endsection
@section('main')
	<div class="row">
		<div class="col-lg-12 margin-tb">
			<div class="pull-left">
				<h2> Novo Utilizador</h2>
			</div>
			<div class="pull-right">
				<a class="btn btn-primary" href="{{ route('users.index') }}"> Voltar</a>
			</div>
		</div>
	</div>

	@if (count($errors) > 0)
		<div class="alert alert-danger">
			<strong>Whoops!</strong> There were some problems with your input.<br><br>
			<ul>
				@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
	@endif

	{!! Form::open(array('id'=>'form','route' => 'users.store','method'=>'POST')) !!}
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12">
			<div class="form-group">
				<strong>Nome:</strong>
				{!! Form::text('name', null, array('placeholder' => 'Nome','class' => 'form-control')) !!}
			</div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-12">
			<div class="form-group">
				<strong>Email:</strong>
				{!! Form::text('email', null, array('placeholder' => 'Email','class' => 'form-control')) !!}
			</div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-12">

			<div class="form-group">
				<strong>Entidade:</strong>
				<select form="form" name="fk_enterpriseID" style="width: 200px" id="typeid">
					<option></option>
					@foreach($enterprise as $c)
						<option value="{{$c->enterpriseID}}">{{$c->enterpriseName}}
						</option>
					@endforeach
				</select>

			</div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-12">
			<div class="form-group">
				<strong>Password:</strong>
				{!! Form::password('password', array('placeholder' => 'Password','class' => 'form-control')) !!}
			</div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-12">
			<div class="form-group">
				<strong>Confirmar Password:</strong>
				{!! Form::password('confirm-password', array('placeholder' => 'Confirmar Password','class' => 'form-control')) !!}
			</div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-12">
			<div class="form-group">
				<strong>Cargo:</strong>
				{!! Form::select('roles[]', $roles,[], array('class' => 'form-control','multiple')) !!}
			</div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-12 text-center">
			<button type="submit" class="btn btn-primary">Submeter</button>
		</div>
	</div>
	{!! Form::close() !!}

@endsection

@section('script')
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.full.js"></script>
	<script src="{{asset('custom/jquery-ui-1.12.1.custom/jquery-ui.min.js')}}"></script>
	<script>
        $("#typeid").select2({
            placeholder: "Escolha uma Entidade",
            allowClear: false
        });
	</script>
@endsection
