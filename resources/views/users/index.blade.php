@extends('layouts.app')
@section('css')
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.css"/>
	<link rel="stylesheet" type="text/css"
	      href="https://cdn.datatables.net/responsive/2.2.2/css/responsive.bootstrap4.min.css"/>
	<link rel="stylesheet" type="text/css"
	      href="{{asset('css/tables-custom.css')}}"/>
@endsection
@section('main')
	<div class="row">
		<div class="col-lg-12 margin-tb">
			<div class="pull-left">
				<h2>Utilizadores</h2>
			</div>
			<div class="pull-right pd5-all">
				<a class="btn btn-success " href="{{ route('users.create') }}"> Novo Utilizador</a>
			</div>
			<div class="pull-right pd5-all">
				<a class="btn btn-success" href="{{ route('users.Enterprise') }}"> Nova Entidade</a>
			</div>
		</div>
	</div>

	@if ($message = Session::get('success'))
		<div class="alert alert-success">
			<p>{{ $message }}</p>
		</div>
	@endif
	@if ($message = Session::get('error'))
		<div class="alert alert-danger">
			<p>{{ $message }}</p>
		</div>
	@endif
	<div class="div-bg-gray ">
		<div class="card-body">
			<table id="key-table" style="width: 100%;" class="table table-striped table-hover dataTable border-table"
			">
			<thead>

			<th>NOME</th>
			<th>ENTIDADE</th>
			<th>EMAIL</th>
			<th>CARGO</th>
			<th width="280px">AÇÃO</th>
			</thead>
			<tbody>
			@foreach ($data as $key => $user)
				<tr>

					<td>{{ $user->name }}</td>
					<td>{{ $user->enterprise['enterpriseName']}}</td>
					<td>{{ $user->email }}</td>
					<td>
						@if(!empty($user->getRoleNames()))
							@foreach($user->getRoleNames() as $v)
								<label class="badge badge-success">{{ $v }}</label>
							@endforeach
						@endif
					</td>
					<td>
						<div class="row">
							{{--<div class="col-lg-4 col-md-4 col-sm-12 pd5">
								<a class="btn btn-info" href="{{ route('users.show',$user->id) }}">Show</a>
							</div>--}}

							<div class="col-lg-4 col-md-4 col-sm-12 pd5">
								@if($user->id == 1)
									@hasrole('Root')
									<div class="pd5-all">
										<a title="Editar!" class="btn  btn-primary" style="padding-top: 3px;
										padding-bottom: 3px;"
										   href="{{ route('users.edit',$user->id) }}"><i
													class="far fa-edit"></i></a>
									</div>
								@else
									<div class="pd5-all">
										<a title="Editar!" class="btn  btn-primary" style="padding-top: 3px;
										padding-bottom: 3px;"
										   href="{{ route('users.edit',$user->id) }}"><i
													class="far fa-edit"></i></a>
									</div>
									@endhasrole
									@else

										<div class="pd5-all">
											<a title="Editar!" class="btn  btn-primary" style="padding-top: 3px;
										padding-bottom: 3px;"
											   href="{{ route('users.edit',$user->id) }}"><i
														class="far fa-edit"></i></a>
										</div>
									@endif
							</div>

							<div class="col-lg-4 col-md-4 col-sm-12">
								{!! Form::open(['method' => 'DELETE','route' => ['users.destroy', $user->id],'style'=>'display:inline']) !!}
								<div class="pd5-all">
									<button title="Apagar!" type="submit"
									        class="btn btn-danger"
									        style="padding-top: 3px;padding-bottom: 3px;"><i
												class="far fa-thumbs-down"></i></button>
								</div>
								{!! Form::close() !!}
							</div>
						</div>
					</td>
				</tr>
			@endforeach
			</tbody>
			</table>

		</div>
	</div>

@endsection
@section('script')
	<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.18/r-2.2.2/datatables.min.js"></script>
	<script type="text/javascript"
	        src="https://cdn.datatables.net/responsive/2.2.2/js/dataTables.responsive.min.js"></script>
	<script type="text/javascript"
	        src="https://cdn.datatables.net/responsive/2.2.2/js/responsive.bootstrap4.min.js"></script>

	<script>
        $(document).ready(function () {
            const table = $('#key-table').DataTable({
                responsive: true,
                bInfo: false,
                bLengthChange: false,
                order: [0, 'desc'],
                stateSave: true,
                "language": {
                    "sEmptyTable": "Nenhum registo encontrado",
                    "sProcessing": "A processar...",
                    "sLengthMenu": "Mostrar _MENU_ registos",
                    "sZeroRecords": "Não foram encontrados resultados",
                    "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registos",
                    "sInfoEmpty": "Mostrando de 0 até 0 de 0 registos",
                    "sInfoFiltered": "(filtrado de _MAX_ registos no total)",
                    "sInfoPostFix": "",
                    "sSearch": "Procurar:",
                    "sUrl": "",
                    "oPaginate": {
                        "sFirst": "Primeiro",
                        "sPrevious": "Anterior",
                        "sNext": "Seguinte",
                        "sLast": "Último"
                    },
                    "oAria": {
                        "sSortAscending": ": Ordenar colunas de forma ascendente",
                        "sSortDescending": ": Ordenar colunas de forma descendente"
                    }
                }
            });
            $("#all").click(function () {
                table.search("").draw();
            });
            $("#mine").click(function () {
                table.search("{{Auth::user()->enterprise->enterpriseName}}").draw();
            });
        });


	</script>
@endsection
