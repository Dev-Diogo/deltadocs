@extends('layouts.app')
@section('css')
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.css"/>
	<link rel="stylesheet" type="text/css"
	      href="https://cdn.datatables.net/responsive/2.2.2/css/responsive.bootstrap4.min.css"/>
	<link rel="stylesheet" type="text/css"
	      href="{{asset('css/tables-custom.css')}}"/>
@endsection
@section('main')
	<div class="row">
		<div class="col-lg-12 margin-tb">
			<div class="pull-right pd5-all">
				<a title="Adicionar Trabalhador" class="btn btn-success" href="{{ route('client.event.addWorker',
				['id'=>$event->eventID])
				}}"><i class="far fa-calendar-plus"></i></a>
			</div>
		</div>
	</div>

	@if ($message = Session::get('success'))
		<div class="alert alert-success">
			<p>{{ $message }}</p>
		</div>
	@endif
	@if ($message = Session::get('error'))
		<div class="alert alert-danger">
			<p>{{ $message }}</p>
		</div>
	@endif
	<h1>{{$event->eventName}}</h1>
	<div class="div-bg-gray ">
		<div class="card-body">
			<table id="key-table" style="width: 100%;"
			       class="table table-striped table-hover dataTable border-table border-table">
				<thead>
				<th>Entidade</th>
				<th>Travablhador</th>
				<th>Email</th>
				<th>Ação</th>
				</thead>
				<tbody>
				@foreach($workers as $worker)
					<tr>
						<td>
							@php
								$user = \App\User::find($worker[0]->workerID);
							@endphp
							{{$user->enterprise['enterpriseName']}}
						</td>
						<td>
							{{$user->name}}
						</td>
						<td>
							{{$user->email}}
						</td>
						<td>
							@if(!isset($isclient))
							@if($event->eventState === '1' || $event->eventState === 1)
								<button onclick="deleteEvent('{{$event->eventID}}')"
								        class="btn btn-outline-danger mr-2"><i
											class="far
														fa-trash-alt"></i></button>
							@endif
							@endif
							@if(!isset($isclient))
							<a href="{{route('client.event.worker.show',['id'=>$event->eventID,
							'workerID'=>$worker[0]->workerID])}}"
							   class="btn btn-outline-success mr-2"><i
										class="far
														fa-window-restore"></i></a>
						</td>
						@else
							<a href="{{route('clientevents.worker',['id'=>$event->eventID,
							'workerID'=>$worker[0]->workerID])}}"
							   class="btn btn-outline-success mr-2"><i
										class="far
														fa-window-restore"></i></a>
						@endif
					</tr>
				@endforeach

				</tbody>
			</table>
			@endsection
			@section('script')

				<script type="text/javascript"
				        src="https://cdn.datatables.net/v/bs4/dt-1.10.18/r-2.2.2/datatables.min.js"></script>
				<script type="text/javascript"
				        src="https://cdn.datatables.net/responsive/2.2.2/js/dataTables.responsive.min.js"></script>
				<script type="text/javascript"
				        src="https://cdn.datatables.net/responsive/2.2.2/js/responsive.bootstrap4.min.js"></script>
				<script type="text/javascript"
				        src="{{asset('js/sweetalert2.all.min.js')}}"></script>
				<script>
                    $(document).ready(function () {
                        var table = $('#key-table').DataTable({
                            responsive: true, bInfo: false, bLengthChange: false,
                            order: [0, 'desc'],
                            stateSave: true,
                            "language": {
                                "sEmptyTable": "Nenhum registo encontrado",
                                "sProcessing": "A processar...",
                                "sLengthMenu": "Mostrar _MENU_ registos",
                                "sZeroRecords": "Não foram encontrados resultados",
                                "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registos",
                                "sInfoEmpty": "Mostrando de 0 até 0 de 0 registos",
                                "sInfoFiltered": "(filtrado de _MAX_ registos no total)",
                                "sInfoPostFix": "",
                                "sSearch": "Procurar:",
                                "sUrl": "",
                                "oPaginate": {
                                    "sFirst": "Primeiro",
                                    "sPrevious": "Anterior",
                                    "sNext": "Seguinte",
                                    "sLast": "Último"
                                },
                                "oAria": {
                                    "sSortAscending": ": Ordenar colunas de forma ascendente",
                                    "sSortDescending": ": Ordenar colunas de forma descendente"
                                }
                            }
                        });

                    });

                    function deleteEvent(id) {
                        swal({
                            title: 'Confirmação?',
                            text: "Tem a certeza de que pretende eliminar este Evento?",
                            type: 'warning',
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Sim, Apagar Evento'
                        }).then((result) => {
                            if (result.value) {
                                $.post("{{route('client.delete.event')}}", {id: id, _token: "{{csrf_token()}}"});
                                swal(
                                    'Apagado!',
                                    'O Evento Foi Apagado.',
                                    'success',
                                ).then(() => {
                                    location.reload();
                                })
                            }
                        })
                    }

                    function showModal(id) {
                        const inputs = document.getElementsByTagName('input');
                        for (let i = 0; i < inputs.length; i++) {
                            inputs[i].disabled = true;
                        }
                        $('#' + id).modal('show');

                    }

                    function toogleState(id) {
                        swal({
                            title: 'Confirmação?',
                            text: "Tem a certeza de que pretende mudar o estado deste Evento?",
                            type: 'warning',
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Sim, Mudar Estado'
                        }).then((result) => {
                            if (result.value) {
                                $.post("{{route('client.event.state')}}", {id: id, _token: "{{csrf_token()}}"});

                                swal(
                                    'Mudado!',
                                    'Estado Mudado Com Sucesso.',
                                    'success',
                                ).then(() => {
                                    location.reload();
                                })
                            }
                        })
                    }
				</script>
@endsection