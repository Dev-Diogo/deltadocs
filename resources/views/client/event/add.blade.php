@extends('layouts.app')
@section('css')
	<link rel="stylesheet" type="text/css"
	      href=" https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css"/>
	<link rel="stylesheet" type="text/css"
	      href="{{asset('custom/jquery-ui-1.12.1.custom/jquery-ui.css')}}"/>
@endsection
@section('main')
	<div class="row">
		<div class="col-lg-12 margin-tb">
			<div class="pull-left">
				<h2> Novo Evento</h2>
			</div>
			<div class="pull-right">
				<a class="btn btn-primary" href="{{ route('client.index') }}"> Voltar</a>
			</div>
		</div>
	</div>

	@if (count($errors) > 0)
		<div class="alert alert-danger">
			<strong>Whoops!</strong> There were some problems with your input.<br><br>
			<ul>
				@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
	@endif

	{!! Form::open(array('id'=>'form','route' => 'client.event.store.worker','method'=>'POST')) !!}
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12">
			<input type="hidden" name="id" value="{{$id}}">
			<div class="form-group">
				<strong>Trabalhador:</strong>
				<select class="form-control" name="worker" id="worker">
					<option></option>
					@foreach($company as $entity)
						<optgroup label="{{$entity->enterpriseName}}">
							@foreach($entity->workers as $worker)

									<option value="{{$worker->id}}">{{$worker->name}}</option>

							@endforeach
						</optgroup>
					@endforeach
				</select>
			</div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-12">
			<input type="hidden" name="id" value="{{$id}}">
			<div class="form-group">
				<strong>Documentos:</strong>
				<select multiple class="form-control" name="docs[]" id="docs">

				</select>
			</div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-12 text-center">
			<button type="submit" class="btn btn-primary">Submeter</button>
		</div>
	</div>
	{!! Form::close() !!}

@endsection

@section('script')
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
	<script>
        $(document).ready(function() {
            $('#worker').select2();
            $('#docs').select2();
        });
        $('#worker').on('select2:select', function (e) {
            const data = e.params.data;
            const id = data.element.value;
            $.post( "{{route('worker.document.get')}}", { id: id , _token: '{{csrf_token()}}' })
                .done(function( data ) {
                    $('#docs').empty().trigger("change");

                   for (i = 0; i < data.length; i++){
                       j = i+1;
                       let newOption = new Option(data[i].documentNameOriginal, data[i].documentNameOriginal, false, true);
                       $('#docs').append(newOption).trigger('change');
                   }
                });
        });

	</script>
	@endsection
