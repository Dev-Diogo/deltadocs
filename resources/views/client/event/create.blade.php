@extends('layouts.app')
@section('css')
	<link rel="stylesheet" type="text/css"
	      href=" https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css"/>
	<link rel="stylesheet" type="text/css"
	      href="{{asset('custom/jquery-ui-1.12.1.custom/jquery-ui.css')}}"/>
@endsection
@section('main')
	<div class="row">
		<div class="col-lg-12 margin-tb">
			<div class="pull-left">
				<h2> Novo Evento</h2>
			</div>
			<div class="pull-right">
				<a class="btn btn-primary" href="{{ route('client.index') }}"> Voltar</a>
			</div>
		</div>
	</div>

	@if (count($errors) > 0)
		<div class="alert alert-danger">
			<strong>Whoops!</strong> There were some problems with your input.<br><br>
			<ul>
				@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
	@endif

	{!! Form::open(array('id'=>'form','route' => 'client.store.event','method'=>'POST')) !!}
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12">
			<input type="hidden" name="id" value="{{$id}}">
			<div class="form-group">
				<strong>Nome do Evento:</strong>
				{!! Form::text('name', null, array('placeholder' => 'Nome','class' => 'form-control')) !!}
			</div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-12">
			<input type="hidden" name="id" value="{{$id}}">
			<div class="form-group">
				<strong>Descrição do Evento:</strong>
				{!! Form::textarea('description', null, array('placeholder' => 'Descrição','class' => 'form-control'))
				 !!}
			</div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-12">
			<div class="form-group">
				<strong>Inicio:</strong>
				<input class="form-control" type="date" name="start">
			</div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-12">
			<div class="form-group">
				<strong>Fim:</strong>
				<input class="form-control" type="date" name="end">
			</div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-12">
			<div class="form-group">
				<strong>Local:</strong>
				<input class="form-control" type="text" name="local">
			</div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-12">
			<div class="form-group">
				<strong>Cliente Final:</strong>
				<input class="form-control" type="text" name="final_client">
			</div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-12 text-center">
			<button type="submit" class="btn btn-primary">Submeter</button>
		</div>
	</div>
	{!! Form::close() !!}

@endsection
