@extends('layouts.app')
@section('css')
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.css"/>
	<link rel="stylesheet" type="text/css"
	      href="https://cdn.datatables.net/responsive/2.2.2/css/responsive.bootstrap4.min.css"/>
	<link rel="stylesheet" type="text/css"
	      href="{{asset('css/tables-custom.css')}}"/>
@endsection
@section('main')
	<div class="row">
		<div class="col-lg-12 margin-tb">
			<div class="pull-right pd5-all">
				<a title="Novo Cliente" class="btn btn-success" href="{{ route('client.create') }}"><i
							class="fas fa-user-plus fa-1x"></i></a>
			</div>
		</div>
	</div>

	@if ($message = Session::get('success'))
		<div class="alert alert-success">
			<p>{{ $message }}</p>
		</div>
	@endif
	@if ($message = Session::get('error'))
		<div class="alert alert-danger">
			<p>{{ $message }}</p>
		</div>
	@endif
	<div class="div-bg-gray ">
		<div class="card-body">
			<table id="key-table" style="width: 100%;"
			       class="table table-striped table-hover dataTable border-table border-table">
				<thead>
				<th>ENTIDADE</th>
				<th>EMAIL</th>
				<th>ESTADO DA CONTA</th>
				<th>AÇÃO</th>
				</thead>
				<tbody>
				@foreach($clients as $client)
					<tr>
						<td>
							{{$client->name}}
						</td>
						<td>
							{{$client->email}}
						</td>
						<td>
							@if($client->state === '1' || $client->state === 1 )
								<p class="badge badge-success">Ativo</p>
							@else
								<p class="badge badge-dark">Inativo</p>
							@endif
						</td>
						<td>
							<button onclick="deleteClient('{{$client->id}}')" class="btn btn-outline-danger mr-2"><i
										class="far
							fa-trash-alt"></i></button>
							<a href="{{route('client.show',$client->id)}}" class="btn btn-outline-info  mr-2"><i
										class="far fa-edit"></i></a>
							<a href="{{route('client.show',$client->id)}}" class="btn btn-outline-success mr-2"><i
										class="far
							fa-window-restore"></i></a>
							@if($client->state === '1' || $client->state === 1)
								<button onclick="toogleState('{{$client->id}}')" class="btn btn-outline-dark"><i
											class="fas fa-user-times"></i></button>
							@else
								<button onclick="toogleState('{{$client->id}}')" class="btn btn-outline-dark"><i
											class="fas fa-user-check"></i></button>
							@endif

						</td>
					</tr>
				@endforeach

				</tbody>
			</table>
			@endsection
			@section('script')

				<script type="text/javascript"
				        src="https://cdn.datatables.net/v/bs4/dt-1.10.18/r-2.2.2/datatables.min.js"></script>
				<script type="text/javascript"
				        src="https://cdn.datatables.net/responsive/2.2.2/js/dataTables.responsive.min.js"></script>
				<script type="text/javascript"
				        src="https://cdn.datatables.net/responsive/2.2.2/js/responsive.bootstrap4.min.js"></script>
				<script type="text/javascript"
				        src="{{asset('js/sweetalert2.all.min.js')}}"></script>
				<script>
                    $(document).ready(function () {
                        var table = $('#key-table').DataTable({
                            responsive: true, bInfo: false, bLengthChange: false,
                            order: [0, 'desc'],
                            stateSave: true,
                            "language": {
                                "sEmptyTable": "Nenhum registo encontrado",
                                "sProcessing": "A processar...",
                                "sLengthMenu": "Mostrar _MENU_ registos",
                                "sZeroRecords": "Não foram encontrados resultados",
                                "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registos",
                                "sInfoEmpty": "Mostrando de 0 até 0 de 0 registos",
                                "sInfoFiltered": "(filtrado de _MAX_ registos no total)",
                                "sInfoPostFix": "",
                                "sSearch": "Procurar:",
                                "sUrl": "",
                                "oPaginate": {
                                    "sFirst": "Primeiro",
                                    "sPrevious": "Anterior",
                                    "sNext": "Seguinte",
                                    "sLast": "Último"
                                },
                                "oAria": {
                                    "sSortAscending": ": Ordenar colunas de forma ascendente",
                                    "sSortDescending": ": Ordenar colunas de forma descendente"
                                }
                            }
                        });

                    });

                    function deleteClient(id) {
                        swal({
                            title: 'Confirmação?',
                            text: "Tem a certeza de que pretende eliminar este colaborador?",
                            type: 'warning',
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Sim, Apagar Colaborador'
                        }).then((result) => {
                            if (result.value) {
                                $.post("{{route('client.delete')}}", {id: id, _token: "{{csrf_token()}}"});
                                swal(
                                    'Apagado!',
                                    'O client Foi Apagado.',
                                    'success',
                                ).then(() => {
                                    location.reload();
                                })
                            }
                        })
                    }

                    function showModal(id) {
                        const inputs = document.getElementsByTagName('input');
                        for (let i = 0; i < inputs.length; i++) {
                            inputs[i].disabled = true;
                        }
                        $('#' + id).modal('show');

                    }

                    function toogleState(id) {
                        swal({
                            title: 'Confirmação?',
                            text: "Tem a certeza de que pretende mudar o estado deste cliente?",
                            type: 'warning',
                            showCancelButton: true,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'Sim, Mudar Estado'
                        }).then((result) => {
                            if (result.value) {
                                $.post("{{route('client.state')}}", {id: id, _token: "{{csrf_token()}}"});

                                swal(
                                    'Mudado!',
                                    'Estado Mudado Com Sucesso.',
                                    'success',
                                ).then(() => {
                                    location.reload();
                                })
                            }
                        })
                    }
				</script>
@endsection