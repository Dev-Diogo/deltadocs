@extends('layouts.app')
@section('css')
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.css"/>
	<link rel="stylesheet" type="text/css"
	      href="https://cdn.datatables.net/responsive/2.2.2/css/responsive.bootstrap4.min.css"/>
	<link rel="stylesheet" type="text/css"
	      href=" https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css"/>
	<link rel="stylesheet" type="text/css"
	      href="{{asset('custom/jquery-ui-1.12.1.custom/jquery-ui.css')}}"/>
	<link rel="stylesheet" type="text/css"
	      href="{{asset('css/tables-custom.css')}}"/>
@endsection
@section('main')
	<style>
		.border300 {
			border-radius: 300px;
		!important;
			padding: 0;
		!important;
			margin: 0 0 0;
		!important;

		}

		.fa25 {
			font-size: 25px;
		!important;
		}
	</style>
	<div class="row">
		<div class="col-lg-12 margin-tb">
			<div class="pull-left">
				<h2>Menu Principal</h2>
			</div>
			<div class="pull-right">
				<button form="form" class="btn btn-primary">Procurar</button>
			</div>
			<div class="pull-right">
				<form id="form" method="POST" action="{{route('dashboard.filter')}}">
					@csrf
					<div id="expire_date" class="col-xs-12 col-sm-12 col-md-12">
						<div class="form-group">
							<input autocomplete="off" form="form" type="text" placeholder="Data de Expiração"
							       class="form-control"
							       name="expire_date" id="datepicker">
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>


	@if ($message = Session::get('success'))
		<div class="alert alert-success">
			<p>{{ $message }}</p>
		</div>
	@endif
	@if ($message = Session::get('error'))
		<div class="alert alert-danger">
			<p>{{ $message }}</p>
		</div>
	@endif

	<div class="div-bg-gray ">
		<div class="card-body">
			<table id="key-table" style="width: 100%;" class="table table-striped table-hover dataTable border-table">
				<thead>
				<th>ENTIDADE</th>
				<th>DOCUMENTOS DA ENTIDADE</th>
				@if($filter == 1)
					<th>DOCUMENTOS EXPIRADOS (ENTIDADE)</th>
				@endif
				<th>DOCUMENTOS DOS COLABORADORES</th>
				@if($filter == 1)
					<th>DOCUMENTOS EXPIRADOS (COLABORADORES)</th>
				@endif


				</thead>
				<tbody>
				@foreach($data as $company)
					<tr>
						<td>
							<button class="btn" style="background-color: Transparent;"
							        onclick="showModal({{$company->enterpriseID}})">{{$company->enterpriseName}}</button>

							<div id="{{$company->enterpriseID}}" name="{{$company->enterpriseID}}"
							     class="modal fade bd-example-modal-lg"
							     tabindex="-1"
							     role="dialog"
							     aria-labelledby="myLargeModalLabel" aria-hidden="true">
								<div class="modal-dialog modal-lg">
									<div class="modal-content">
										<div class="modal-header text-black-50">{{$company->enterpriseName}}</div>
										<div class="pull-right mgr-25">
											<button class="btn btn-success
										pull-right" onclick="btntoogle({{$company->enterpriseID}})">Editar
											</button>
										</div>
										<div class="card">

											<div class="card-body">
												<div class="row">
													<form method="POST" action="{{route('companies.info.update')}}"
													      id="{{$company->enterpriseID}}{{$company
													->enterpriseName}}"
													      name="id="{{$company->enterpriseID}}{{$company->enterpriseName}}
													""
													class="mgl-25 mgr-25 width-100">
													@csrf
													<div class="form-group">
														<label for="id">Identificador:</label>
														<input type="id" name="id" class="form-control" id="id"
														       placeholder="{{$company->enterpriseID}}"
														       value="{{$company->enterpriseID}}"
														       disabled>
													</div>
													<div class="form-group">
														<label for="name">Nome:</label>
														<input type="text" name="name" class="form-control"
														       id="name"
														       placeholder="{{$company->enterpriseName}}"
														       value="{{$company->enterpriseName}}"
														       disabled>
													</div>
													<div class="form-group">
														<label for="name">NIPC:</label>
														<input type="text" name="nif" class="form-control"
														       id="nif"
														       placeholder="{{$company->enterpriseNif}}"
														       value="{{$company->enterpriseNif}}"
														       disabled>
													</div>
													<div class="form-group">
														<label for="name">Morada:</label>
														<input type="text" name="address" class="form-control"
														       id="address"
														       placeholder="{{$company->enterpriseAddress}}"
														       value="{{$company->enterpriseAddress}}"
														       disabled>
													</div>
													<div class="form-group">
														<label for="name">Telefone:</label>
														<input type="text" name="telephone" class="form-control"
														       id="telephone"
														       placeholder="{{$company->enterpriseTelephone}}"
														       value="{{$company->enterpriseTelephone}}"
														       disabled>
													</div>
													<div id="btndiv{{$company->enterpriseID}}" style="display: none;">
														<button onclick="submit({{$company->enterpriseID}}{{$company->enterpriseName}}
																)" class="btn">Submit
														</button>
													</div>
													</form>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
		</div>
		</td>
		<td>
			@php
				$docs = \App\CompanyDocs::all()->where('company_id','=',$company->enterpriseID);
				$count_pend = $docs->where('valid','=','0')->count();
				$count_den = $docs->where('valid','=','2')->count();
				$count_app = $docs->where('valid','=','1')->count();
				$count_exp = $docs->where('expires_at','<',\Carbon\Carbon::today('Europe/Lisbon'))->count();
				$count_exp_6month = $docs->where('valid','=','1')->where('expires_at','<',
									\Carbon\Carbon::today()->addMonth(6))->count();
				if($count_den >0 or $count_exp > 0){
				echo'<a class="border300 btn btn-danger "
				href="'.route("company.filter",$company->enterpriseID).'"><i
				   class="fa25 fas fa-times-circle 1"></i></a>';
				}else if($count_pend >0){
				echo'<a class="border300 btn btn-warning " s
				href="'.route("company.filter",$company->enterpriseID).'"><i class="fa25 fas fa-exclamation-circle fa-1x"></i></a>';
				}else if($count_app >0){
				if($count_exp_6month >0){
				echo'<a class="border300 btn btn-warning "
				href="'.route("company.filter",$company->enterpriseID).'"><i
				   class="fa25 fas fa-check fa-1x"></i></a>';
				}else{
				echo'<a class="border300 btn btn-success "
				href="'.route("workers.filter",$company->enterpriseID).'"><i
				   class="fa25 fas fa-check fa-1x"></i></a>';
				}
				}else{
				echo'<a class="border300 btn btn-info "
				href="'.route("company.filter",$company->enterpriseID).'">
				<i class="fa25 fas fa-plus-circle"></i></a>';

				//echo('<label class="badge badge-warning pd5-all text-16"><i class="fas fa-file-upload fa-2x"></i></label>');
				}
			@endphp
		</td>
		@if($filter == 1)
			<td>
				@php
					$count = \App\CompanyDocs::all()->where('company_id','=',$company->enterpriseID)->where
					('expires_at','!=',NULL)->where('expires_at','<=',$date)->count();
					echo'<a class="btn btn-primary "
					href="'.route("dashboard.company.filter",[$company->enterpriseID,$date]).'">'.$count.'</a>';
				@endphp
			</td>
		@endif

		<td>
			@php
				$docs = \App\WorkerDocs::all()->where('company_id','=',$company->enterpriseID);
				$count_pend = $docs->where('valid','=','0')->count();
				$count_den = $docs->where('valid','=','2')->count();
				$count_app = $docs->where('valid','=','1')->count();
				$count_exp = $docs->where('expires_at','<',\Carbon\Carbon::today('Europe/Lisbon'))->where('valid','=',
				'1')
				->count();
				$count_exp_6month = $docs->where('valid','=','1')->where('expires_at','<',
									Carbon\Carbon::today()->addMonth(6))->count();
		if($count_den >0 or $count_exp > 0){
				echo'<a class="border300 btn btn-danger "
				href="'.route("workers.filter",$company->enterpriseID).'"><i
				   class="fa25 fas fa-times-circle 1"></i></a>';
				}else if($count_pend >0){
				echo'<a class="border300 btn btn-warning " s
				href="'.route("workers.filter",$company->enterpriseID).'"><i class="fa25 fas fa-exclamation-circle fa-1x"></i></a>';
				}else if($count_app >0){
				if($count_exp_6month >0){
				echo'<a class="border300 btn btn-warning "
				href="'.route("workers.filter",$company->enterpriseID).'"><i
				   class="fa25 fas fa-check fa-1x"></i></a>';
				}else{
				echo'<a class="border300 btn btn-success "
				href="'.route("workers.filter",$company->enterpriseID).'"><i
				   class="fa25 fas fa-check fa-1x"></i></a>';
				}

				}else{
				echo'<a class="border300 btn btn-info "
				href="'.route("workers.filter",$company->enterpriseID).'">
				<i class="fa25 fas fa-plus-circle"></i></a>';

				//echo('<label class="badge badge-warning pd5-all text-16"><i class="fas fa-file-upload fa-2x"></i></label>');
				}
			@endphp
		</td>
		@if($filter == 1)
			<td>
				@php
					$count = \App\WorkerDocs::all()->where('company_id','=',$company->enterpriseID)->where('expires_at','!=',NULL)->where('expires_at','<=',$date)->count();

				echo'<a class=" btn btn-primary "
				href="'.route("dashboard.worker.filter",[$company->enterpriseID,$date]).'">'.$count.'</a>';
				@endphp
			</td>
			@endif

			</tr>
			@endforeach
			</tbody>
			</table>
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12">
					<div class="pull-left">
						<h4>Legenda</h4>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-2">
					<div>
						<strong>
							<a class="border300 btn btn-success"><i class="fa25 fas fa-check fa-1x"></i></a>&nbsp;Válido
						</strong>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-2">
					<div>
						<strong>
							<a class="border300 btn btn-warning"><i class="fa25 fas fa-check fa-1x"></i></a>&nbsp;
							Validade Reduzida
						</strong>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-2">
					<div>
						<strong>
							<a class="border300 btn btn-warning"><i class="fa25 fas
							fa-exclamation-circle"></i></a>&nbsp;Pendente
						</strong>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-2">
					<div>
						<strong>
							<a class="border300 btn btn-danger"><i class="fa25 fas fa-times-circle"></i></a>&nbsp;Inválido
						</strong>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-2">
					<div>
						<strong>
							<a class="border300 btn btn-info"><i class="fa25 fas fa-plus-circle"></i></a>&nbsp;Vazio
						</strong>
					</div>
				</div>
			</div>
	</div>
	</div>




@endsection
@section('script')

	<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.18/r-2.2.2/datatables.min.js"></script>
	<script type="text/javascript"
	        src="https://cdn.datatables.net/responsive/2.2.2/js/dataTables.responsive.min.js"></script>
	<script type="text/javascript"
	        src="https://cdn.datatables.net/responsive/2.2.2/js/responsive.bootstrap4.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.full.js"></script>
	<script src="{{asset('custom/jquery-ui-1.12.1.custom/jquery-ui.min.js')}}"></script>
	<script>
        $(document).ready(function () {
            $('#key-table').DataTable({
                responsive: true,
                bInfo: false,
                bLengthChange: false,
                order: [0, 'desc'],
                "language": {
                    "sEmptyTable": "Nenhum registo encontrado",
                    "sProcessing": "A processar...",
                    "sLengthMenu": "Mostrar _MENU_ registos",
                    "sZeroRecords": "Não foram encontrados resultados",
                    "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registos",
                    "sInfoEmpty": "Mostrando de 0 até 0 de 0 registos",
                    "sInfoFiltered": "(filtrado de _MAX_ registos no total)",
                    "sInfoPostFix": "",
                    "sSearch": "Procurar:",
                    "sUrl": "",
                    "oPaginate": {
                        "sFirst": "Primeiro",
                        "sPrevious": "Anterior",
                        "sNext": "Seguinte",
                        "sLast": "Último"
                    },
                    "oAria": {
                        "sSortAscending": ": Ordenar colunas de forma ascendente",
                        "sSortDescending": ": Ordenar colunas de forma descendente"
                    }


                }
            });
        });
        $(function () {
            $("#datepicker").datepicker({
                dateFormat: 'yy-mm-dd',
                changeMonth: true,
                changeYear: true
            });
        });
	</script>

	<script>

        function showModal(id) {
            const x = document.getElementById("btndiv" + id);
            x.style.display = "none";
            const inputs = document.getElementsByTagName('input');
            for (let i = 0; i < inputs.length; i++) {
                inputs[i].disabled = true;
            }
            document.getElementById("datepicker").disabled = false;
            $("input", "#key-table_filter").removeAttr('disabled');
            $('#' + id).modal('show');

        }

        function btntoogle(id) {
            const inputs = document.getElementsByTagName('input');

            const x = document.getElementById("btndiv" + id);
            if (x.style.display === "none") {
                x.style.display = "block";
                for (let i = 1; i < inputs.length; i++) {
                    inputs[i].disabled = false;
                }
                document.getElementById("datepicker").disabled = false;
                $("input", "#key-table_filter").removeAttr('disabled');
            } else {
                x.style.display = "none";
                for (let i = 0; i < inputs.length; i++) {
                    inputs[i].disabled = true;
                }
                document.getElementById("datepicker").disabled = false;
                $("input", "#key-table_filter").removeAttr('disabled');
            }
        }

        function submit(id) {
            $("#" + id).submit();
        }

	</script>

@endsection


