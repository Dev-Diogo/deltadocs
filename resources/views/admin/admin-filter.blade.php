@extends('layouts.app')
@section('css')
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.css"/>
	<link rel="stylesheet" type="text/css"
	      href="https://cdn.datatables.net/responsive/2.2.2/css/responsive.bootstrap4.min.css"/>
	<link rel="stylesheet" type="text/css"
	      href="{{asset('css/tables-custom.css')}}"/>
@endsection
@section('main')
	<div class="row">
		<div class="col-lg-12 margin-tb">
			<div class="pull-left">
				<h2>Documentos</h2>
			</div>
		</div>
	</div>

	@if ($message = Session::get('success'))
		<div class="alert alert-success">
			<p>{{ $message }}</p>
		</div>
	@endif
	@if ($message = Session::get('error'))
		<div class="alert alert-danger">
			<p>{{ $message }}</p>
		</div>
	@endif
	<div class="div-bg-gray ">
		<div class="card-body">
			<table id="key-table" style="width: 100%;" class="table table-striped table-hover dataTable border-table">
			<thead>
			<th>ENTIDADE</th>
			<th>DOCUMENTO</th>
			<th>ESTADO</th>
			<th>VALIDADE</th>
			</thead>
			<tbody>
			@foreach($docs as $doc)
				@if($user->enterprise->enterpriseID == $doc->company_id or $user->enterprise->enterpriseID == 1)
				<tr>

					<td>
						<button class="btn"
						        onclick="showModal({{$company->enterpriseID}})">{{$company->enterpriseName}}</button>

						<div id="{{$company->enterpriseID}}" name="{{$company->enterpriseID}}"
						     class="modal fade bd-example-modal-lg"
						     tabindex="-1"
						     role="dialog"
						     aria-labelledby="myLargeModalLabel" aria-hidden="true">
							<div class="modal-dialog modal-lg">
								<div class="modal-content">
									<div class="modal-header text-black-50">{{$company->enterpriseName}}</div>
									<div class="pull-right mgr-25">
										<button class="btn btn-success
										pull-right" onclick="btntoogle({{$company->enterpriseID}})">Editar
										</button>
									</div>
									<div class="card">

										<div class="card-body">
											<div class="row">
												<form method="POST" action="{{route('companies.info.update')}}"
												      id="{{$company->enterpriseID}}{{$company
													->enterpriseName}}"
												      name="id="{{$company->enterpriseID}}{{$company->enterpriseName}}

												      class="mgl-25 mgr-25 width-100">
												@csrf
												<div class="form-group">
													<label for="id">Identificador:</label>
													<input type="id" name="id" class="form-control" id="id"
													       placeholder="{{$company->enterpriseID}}"
													       value="{{$company->enterpriseID}}"
													       disabled>
												</div>
												<div class="form-group">
													<label for="name">Nome:</label>
													<input type="text" name="name" class="form-control"
													       id="name"
													       placeholder="{{$company->enterpriseName}}"
													       value="{{$company->enterpriseName}}"
													       disabled>
												</div>
												<div class="form-group">
													<label for="name">Nif:</label>
													<input type="text" name="nif" class="form-control"
													       id="nif"
													       placeholder="{{$company->enterpriseNif}}"
													       value="{{$company->enterpriseNif}}"
													       disabled>
												</div>
												<div class="form-group">
													<label for="name">Morada:</label>
													<input type="text" name="address" class="form-control"
													       id="address"
													       placeholder="{{$company->enterpriseAddress}}"
													       value="{{$company->enterpriseAddress}}"
													       disabled>
												</div>
												<div id="btndiv{{$company->enterpriseID}}" style="display: none;">
													<button onclick="submit
															({{$company->enterpriseID}}{{$company->enterpriseName}}
															)" class="btn">Submit
													</button>
												</div>
												</form>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
		</div>
		</td>
		</td>
		<td>
			<div class="pd5-all">
				<form method="post" action="{{route('company.document.get')}}">
					@csrf
					<input type="hidden" id="doc" name="doc"
					       value="{{$doc->documentName}}.{{$doc->document_extension}}">
					<input type="hidden" id="id" name="id" value="{{$doc->company_id}}">
					<input type="hidden" id="hash" name="hash"
					       value="@php use Carbon\Carbon;echo(Illuminate\Support\Facades\Hash::make( Carbon::today() && $doc->documentName && $user->email && $user->enterprise->enterpriseName));@endphp">
					<button class="btn btn-info "
					        style="padding-top: 3px;padding-bottom: 3px;">@if($doc->document_extension == 'pdf')
							<i
									class="far fa-file-pdf"></i>&ensp;{{$doc->types->typeName}}

						@elseif($doc->document_extension == 'docx')
							<i
									class="far fa-file-word"></i>
							&ensp;{{$doc->types->typeName}}</label>
						@elseif($doc->document_extension == 'docx')<i
									class="far far fa-file-excel"></i>
							&ensp;{{$doc->types->typeName}}</label>
						@else<i
									class="far far fa-file"></i>
							&ensp;{{$doc->types->typeName}}</label>

						@endif</button>
				</form>
			</div>
		</td>
		<td>
			<div class="pd5-all">
				@php
					$valid= $doc->valid;
					$expires_at = $doc->expires_at;

				if($valid == 2) {
				echo('<label data-toggle="tooltip" title="'.$doc->description .'" class="badge badge-danger pd5-all text-16"><i class="fas fa-times-circle fa-1x"></i></label>');
				 }else if($expires_at < Carbon\Carbon::today('Europe/Lisbon') && $expires_at != NULL ){
			   echo('<label data-toggle="tooltip" title="Documento Expirado" class="badge badge-danger pd5-all text-16"><i class="fas fa-times-circle fa-1x"></i></label>');
				   }
				else if($valid == 0)
				{
				echo('<label class="badge badge-info  text-16"><i class="fas fa-exclamation-circle fa-1x"></i></label>');
				   }else if($valid == 1) {
				echo('<label class="badge badge-success  text-16"><i class="fas fa-check fa-1x"></i></label>');
			   }

				@endphp
			</div>

		</td>
		<td>
			<div class="pd5-all">

				@php
					$expire= $doc->expires_at;

				iF($expire == NULL)
				{
				 echo('<label class="badge badge-info "><i class="fas fa-ban fa-2x"></i></label>');
				}else if($expire < Carbon\Carbon::today('Europe/Lisbon') && $expires_at != NULL){
				echo('<label class="badge badge-danger text-16">'.$expire.'</label>');
				}else if($expire < Carbon\Carbon::today('Europe/Lisbon')->addMonth(1) && $expires_at != NULL){
				echo('<label class="badge badge-warning text-16">'.$expire.'</label>');
				}
				else{
				echo('<label class="badge badge-info text-16">'.$expire.'</label>');
				}
				@endphp
			</div>

		</td>
		</tr>
		@endif
		@endforeach
		</tbody>
		</table>

		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12">
				<div class="pull-left">
					<h4>Legenda</h4>
				</div>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-3">
				<div>
					<strong>
						<a class="border300 btn btn-success"><i class="fa25 fas fa-check fa-1x"></i></a>&nbsp;Ficheiros
						Validos
					</strong>
				</div>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-3">
				<div>
					<strong>
						<a class="border300 btn btn-warning"><i class="fa25 fas fa-exclamation-circle"></i></a>&nbsp;Ficheiros
						Pendentes
					</strong>
				</div>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-3">
				<div>
					<strong>
						<a class="border300 btn btn-danger"><i class="fa25 fas fa-times-circle"></i></a>&nbsp;Ficheiros
						com Problemas
					</strong>
				</div>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-3">
				<div>
					<strong>
						<a class="border300 btn btn-info"><i class="fa25 fas fa-plus-circle"></i></a>&nbsp;Sem
						Ficheiros
					</strong>
				</div>
			</div>
		</div>
	</div>
	</div>


@endsection
@section('script')

	<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.18/r-2.2.2/datatables.min.js"></script>
	<script type="text/javascript"
	        src="https://cdn.datatables.net/responsive/2.2.2/js/dataTables.responsive.min.js"></script>
	<script type="text/javascript"
	        src="https://cdn.datatables.net/responsive/2.2.2/js/responsive.bootstrap4.min.js"></script>

	<script>
        $(document).ready(function () {
            var table = $('#key-table').DataTable({
                responsive: true, bInfo: false, bLengthChange: false,
                order: [0, 'desc'],
                "language": {
                    "sEmptyTable": "Nenhum registo encontrado",
                    "sProcessing": "A processar...",
                    "sLengthMenu": "Mostrar _MENU_ registos",
                    "sZeroRecords": "Não foram encontrados resultados",
                    "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registos",
                    "sInfoEmpty": "Mostrando de 0 até 0 de 0 registos",
                    "sInfoFiltered": "(filtrado de _MAX_ registos no total)",
                    "sInfoPostFix": "",
                    "sSearch": "Procurar:",
                    "sUrl": "",
                    "oPaginate": {
                        "sFirst": "Primeiro",
                        "sPrevious": "Anterior",
                        "sNext": "Seguinte",
                        "sLast": "Último"
                    },
                    "oAria": {
                        "sSortAscending": ": Ordenar colunas de forma ascendente",
                        "sSortDescending": ": Ordenar colunas de forma descendente"
                    },
                }
            });
            $("#all").click(function () {
                table.search("").draw();
            });
            $("#mine").click(function () {
                table.search("{{$user->enterprise->enterpriseName}}").draw();
            });
        });


	</script>
	<script>
        $(document).ready(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });

        function showModal(id) {
            const x = document.getElementById("btndiv" + id);
            x.style.display = "none";
            const inputs = document.getElementsByTagName('input');
            for (let i = 0; i < inputs.length; i++) {
                inputs[i].disabled = true;
            }
            document.getElementById("datepicker").disabled = false;
            $("input", "#key-table_filter").removeAttr('disabled');
            $('#' + id).modal('show');

        }

        function btntoogle(id) {
            const inputs = document.getElementsByTagName('input');

            const x = document.getElementById("btndiv" + id);
            if (x.style.display === "none") {
                x.style.display = "block";
                for (let i = 1; i < inputs.length; i++) {
                    inputs[i].disabled = false;
                }
                document.getElementById("datepicker").disabled = false;
                $("input", "#key-table_filter").removeAttr('disabled');
            } else {
                x.style.display = "none";
                for (let i = 0; i < inputs.length; i++) {
                    inputs[i].disabled = true;
                }
                document.getElementById("datepicker").disabled = false;
                $("input", "#key-table_filter").removeAttr('disabled');
            }
        }

        function submit(id) {
            $("#" + id).submit();
        }

	</script>

@endsection