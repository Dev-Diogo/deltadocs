<div id="wrapper">

	<!-- ========== Left Sidebar Start ========== -->
	<!-- Left Sidebar End -->
	<div class="left side-menu side-menu-sm">

		<div class="slimscroll-menu" id="remove-scroll">

			<!-- LOGO -->
			<div class="topbar-left">
				<a href="/" class="logo">
                            <span>
                                <img src="{{asset('images/logo.png')}}" alt="" height="50">
                            </span>
					<i>
						<img src="{{asset('images/logo.png')}}" alt="" height="28">
					</i>
				</a>
			</div>

			<!-- User box -->
			<div class="user-box">
				<div class="user-img">
					<img src="{{asset('images/user/default/avatar.png')}}" alt="user-img" title="Mat Helme"
					     class="rounded-circle img-fluid">
				</div>
				<h5><a href="#">{{{ isset(Auth::user()->name) ? Auth::user()->name : Auth::user()->email }}}</a></h5>

			</div>

			<!--- Sidemenu -->
			<div id="sidebar-menu">

				<ul class="metismenu" id="side-menu">


					<li>
						@can('administrator')
							<a href="/admin">
								@elsecan('manager')
									<a href="/dashboard">
										@else
											<a href="/dashboard">
												@endcan
												<i class="fi-air-play"></i><span
														class="badge badge-danger badge-pill pull-right">7</span><span> Menu Principal </span></a>

					</li>

					@can('user-list')
						<li>
							<a href="/users">
								<i class="fi-paper"></i><span class="badge badge-danger badge-pill pull-right">7</span>
								<span> Utilizadores </span>
							</a>
						</li>
					@endcan
					@can('manager')
						<li>
							<a href="/workers">
								<i class="fi-paper"></i><span class="badge badge-danger badge-pill pull-right">7</span>
								<span> Colaboradores </span>
							</a>
						</li>
					@endcan
					@can('administrator2')
						<li>
							<a href="/otherworkers">
								<i class="fi-paper"></i><span class="badge badge-danger badge-pill pull-right">7</span>
								<span> Outros Colaboradores </span>
							</a>
						</li>
					@endcan


					@can('manager')
						<li>
							<a href="/company">
								<i class="fi-paper"></i><span class="badge badge-danger badge-pill pull-right">7</span>
								@if(auth()->user()->fk_enterpriseID != 1)
									<span> Entidade </span>
								@else
									<span> Entidades </span>
								@endif
							</a>
						</li>
					@endcan
					@can('user-list')
						<li>
							<a href="/client">
								<i class="fas fa-users-cog"></i><span
										class="badge badge-danger badge-pill pull-right">7</span>
								<br>
								<span> Gerir Clientes </span>
							</a>
						</li>
					@endcan
				</ul>

			</div>
			<!-- Sidebar -->

			<div class="clearfix"></div>

		</div>
		<!-- Sidebar -left -->

	</div>
	<!-- Left Sidebar End -->


	<!-- ============================================================== -->
	<!-- Start right Content here -->
	<!-- ============================================================== -->

	<div class="content-page">

		<!-- Top Bar Start -->
		<div class="topbar">

			<nav class="navbar-custom">

				<ul class="list-unstyled topbar-right-menu float-right mb-0">


					<li class="dropdown notification-list">
						<a class="nav-link dropdown-toggle nav-user" data-toggle="dropdown" href="#" role="button"
						   aria-haspopup="false" aria-expanded="false">
							<img src="{{asset('images/user/default/avatar.png')}}" alt="user" class="rounded-circle">
							<span class="ml-1">{{{ isset(Auth::user()->name) ? Auth::user()->name : Auth::user()->email }}}
								<i class="mdi mdi-chevron-down"></i> </span>
						</a>
						<div class="dropdown-menu dropdown-menu-right dropdown-menu-animated profile-dropdown ">
							<!-- item-->
							<div class="dropdown-item noti-title">
								<h6 class="text-overflow m-0">
									Bem
									Vindo {{{ isset(Auth::user()->name) ? Auth::user()->name : Auth::user()->email }}}
									!</h6>
							</div>

							<!-- item-->


							<!-- item-->
							<a class="dropdown-item notify-item" href="{{ route('logout') }}"
							   onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
								{{ __('Sair') }}
							</a>
							<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
								@csrf
							</form>

						</div>
					</li>

				</ul>

				<ul class="list-inline menu-left mb-0">
					<li class="hide float-left">
						<button class="hide button-menu-mobile open-left">
							<i class=" dripicons-menu"></i>
						</button>
					</li>

				</ul>

			</nav>

		</div>
		<!-- Top Bar End -->


		<!-- Start Page content -->
		<div class="content">
			<div class="container-fluid">
				@yield('main')

			</div> <!-- content -->

			<footer class="footer text-right">
				2018 © Visual Thinking.
			</footer>

		</div>


		<!-- ============================================================== -->
		<!-- End Right content here -->
		<!-- ============================================================== -->


	</div>

	</body>
