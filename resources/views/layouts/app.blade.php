<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width,initial-scale=1">

	<!-- CSRF Token -->
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<title>{{ config('app.name', 'Laravel') }}</title>

	<!-- Scripts -->

	<!-- Fonts -->
	<link rel="dns-prefetch" href="https://fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">
	<!-- Styles -->
	<link href="{{asset('css/bootstrap.css')}}" rel="stylesheet" type="text/css"/>
	<link href="{{asset('css/metismenu.min.css')}}" rel="stylesheet" type="text/css"/>
	<link href="{{asset('css/style.css')}}" rel="stylesheet" type="text/css"/>
	<link href="{{asset('css/style_dark.css')}}" rel="stylesheet" type="text/css"/>
	@yield('css')
	<link rel="stylesheet" href="{{asset('css/dropzone.css')}}">
	<link href="{{asset('css/icons.css')}}" rel="stylesheet" type="text/css"/>
	<link href="{{asset('css/fontawsome.css')}}" rel="stylesheet" type="text/css"/>
	<link href="{{asset('css/custom.css')}}" rel="stylesheet" type="text/css"/>
	<!-- App js -->


</head>
<body>
<div id="app">

	@guest
		<main>
			@yield('content')
		</main>

	@else

		@include('layouts.include.nav')
	@endguest
</div>
<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="{{asset('js/fontawsome.js')}}"></script>
<script type="text/javascript" src="{{asset('js/modernizr.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/popper.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/bootstrap.js')}}"></script>

<script type="text/javascript" src="{{asset('js/metisMenu.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/waves.js')}}"></script>
<script type="text/javascript" src="{{asset('js/jquery.slimscroll.js')}}"></script>
<script type="text/javascript" src="{{asset('js/dropzone.js')}}"></script>
@yield('script')
<script src="{{asset('js/jquery.core.js')}}"></script>
<script src="{{asset('js/jquery.app.js')}}"></script>

</body>
</html>
