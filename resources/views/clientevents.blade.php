@extends('layouts.app')
@section('css')
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.css"/>
	<link rel="stylesheet" type="text/css"
		  href="https://cdn.datatables.net/responsive/2.2.2/css/responsive.bootstrap4.min.css"/>
	<link rel="stylesheet" type="text/css"
		  href="{{asset('css/tables-custom.css')}}"/>
@endsection
@section('main')


	@if ($message = Session::get('success'))
		<div class="alert alert-success">
			<p>{{ $message }}</p>
		</div>
	@endif
	@if ($message = Session::get('error'))
		<div class="alert alert-danger">
			<p>{{ $message }}</p>
		</div>
	@endif
	<h1>Eventos</h1>
	<div class="div-bg-gray ">
		<div class="card-body">
			<table id="key-table" style="width: 100%;"
				   class="table table-striped table-hover dataTable border-table border-table">
				<thead>
				<th>Evento</th>
				<th>Cliente Final</th>
				<th>Local</th>
				<th>Descrição</th>
				<th>Inicio Do Evento</th>
				<th>Fim Do Evento</th>
				<th>Estado</th>
				<th>Ação</th>
				</thead>
				<tbody>
				@foreach($events as $event)

					<tr>
						<td>
							{{$event->eventID}}
						</td>
						<td>
							{{$event->final_client}}
						</td>
						<td>
							{{$event->local}}
						</td>
						<td>
							{{$event->eventDescription}}
						</td>
						<td>
							{{$event->eventStartDate}}
						</td>
						<td>
							{{$event->eventEndDate}}
						</td>
						<td>
							@if($event->eventState === '1' || $event->eventState === 1)
								<p class="badge badge-success">Em Curso</p>
							@else
								<p class="badge badge-dark">Concluido</p>
							@endif
						</td>
						<td>
							<a href="{{route('clientevents.show',$event->eventID)}}"
							   class="btn btn-outline-success mr-2"><i
										class="far
							fa-window-restore"></i></a>
						</td>
					</tr>
				@endforeach

				</tbody>
			</table>
			@endsection
			@section('script')

				<script type="text/javascript"
						src="https://cdn.datatables.net/v/bs4/dt-1.10.18/r-2.2.2/datatables.min.js"></script>
				<script type="text/javascript"
						src="https://cdn.datatables.net/responsive/2.2.2/js/dataTables.responsive.min.js"></script>
				<script type="text/javascript"
						src="https://cdn.datatables.net/responsive/2.2.2/js/responsive.bootstrap4.min.js"></script>
				<script type="text/javascript"
						src="{{asset('js/sweetalert2.all.min.js')}}"></script>
				<script>
					$(document).ready(function () {
						var table = $('#key-table').DataTable({
							responsive: true, bInfo: false, bLengthChange: false,
							order: [0, 'desc'],
							stateSave: true,
							"language": {
								"sEmptyTable": "Nenhum registo encontrado",
								"sProcessing": "A processar...",
								"sLengthMenu": "Mostrar _MENU_ registos",
								"sZeroRecords": "Não foram encontrados resultados",
								"sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registos",
								"sInfoEmpty": "Mostrando de 0 até 0 de 0 registos",
								"sInfoFiltered": "(filtrado de _MAX_ registos no total)",
								"sInfoPostFix": "",
								"sSearch": "Procurar:",
								"sUrl": "",
								"oPaginate": {
									"sFirst": "Primeiro",
									"sPrevious": "Anterior",
									"sNext": "Seguinte",
									"sLast": "Último"
								},
								"oAria": {
									"sSortAscending": ": Ordenar colunas de forma ascendente",
									"sSortDescending": ": Ordenar colunas de forma descendente"
								}
							}
						});

					});

					function deleteEvent(id) {
						swal({
							title: 'Confirmação?',
							text: "Tem a certeza de que pretende eliminar este Evento?",
							type: 'warning',
							showCancelButton: true,
							confirmButtonColor: '#3085d6',
							cancelButtonColor: '#d33',
							confirmButtonText: 'Sim, Apagar Evento'
						}).then((result) => {
							if (result.value) {
								$.post("{{route('client.delete.event')}}", {id: id, _token: "{{csrf_token()}}"});
								swal(
										'Apagado!',
										'O Evento Foi Apagado.',
										'success',
								).then(() => {
									location.reload();
								})
							}
						})
					}

					function showModal(id) {
						const inputs = document.getElementsByTagName('input');
						for (let i = 0; i < inputs.length; i++) {
							inputs[i].disabled = true;
						}
						$('#' + id).modal('show');

					}

					function toogleState(id) {
						swal({
							title: 'Confirmação?',
							text: "Tem a certeza de que pretende mudar o estado deste Evento?",
							type: 'warning',
							showCancelButton: true,
							confirmButtonColor: '#3085d6',
							cancelButtonColor: '#d33',
							confirmButtonText: 'Sim, Mudar Estado'
						}).then((result) => {
							if (result.value) {
								$.post("{{route('client.event.state')}}", {id: id, _token: "{{csrf_token()}}"});

								swal(
										'Mudado!',
										'Estado Mudado Com Sucesso.',
										'success',
								).then(() => {
									location.reload();
								})
							}
						})
					}
				</script>
@endsection