@extends('layouts.app')
@section('css')
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.css"/>
	<link rel="stylesheet" type="text/css"
	      href="https://cdn.datatables.net/responsive/2.2.2/css/responsive.bootstrap4.min.css"/>
	<link rel="stylesheet" type="text/css"
	      href="{{asset('css/tables-custom.css')}}"/>
@endsection
@section('main')
	<style>
		.border300 {
			border-radius: 300px;
		!important;
			padding: 0;
		!important;
			margin: 0 0 0;
		!important;

		}

		.fa25 {
			font-size: 25px;
		!important;
		}
	</style>
	<div class="row">
		<div class="col-lg-12 margin-tb">
			<div class="pull-left">
				@if($filter == 1)
					<h2>{{$enterprise->enterpriseName}}</h2>
				@else
					<h2>Documentos</h2>
				@endif
			</div>
			@if($filter == 1)
				<div class="col-lg-12 margin-tb">
					<div class="pull-right pd5-all ">
						<a class="btn btn-primary" href="{{ route('admin.index') }}"> Voltar</a>
					</div>
				</div>
			@endif
			<div class="pull-right pd5-all">
				<a title="Submeter" class="btn btn-success " href="{{ route('company.document.upload') }}"><i
							class="fas fa-file-upload fa-1x"></i></a>
			</div>
			@can('manager')
				@if(Auth::user()->enterprise->enterpriseID ==1)
					<div class="col-lg-12 margin-tb">
						<div class="pull-right pd5-all">
							<a title="Adicionar" class="btn btn-success" href="{{ route('company.create') }}"><i
										class="far fa-plus-square fa-1x"></i></a>
						</div>
					</div>
					<div class="col-lg-12 margin-tb">
						<div class="pull-right pd5-all">
							<a title="Remover" class="btn btn-success" href="{{ route('company.deleteview')}}"><i
										class="far fa-minus-square fa-1x"></i></a>
						</div>
					</div>
					@if($filter == 0)
						<div class="col-lg-12 margin-tb">
							<div class="pull-right pd5-all">
								<a id="all" name="all" value="{{$user->enterprise->enterpriseName}}"
								   class="btn btn-success">Todos</a>
							</div>
						</div>
						<div class="col-lg-12 margin-tb">
							<div class="pull-right pd5-all">
								<a id="mine" name="mine" value="{{$user->enterprise->enterpriseName}}"
								   class="btn btn-success">Interno</a>
							</div>
						</div>

					@endif
				@endif
			@endcan
		</div>
	</div>

	@if ($message = Session::get('success'))
		<div class="alert alert-success">
			<p>{{ $message }}</p>
		</div>
	@endif
	@if ($message = Session::get('error'))
		<div class="alert alert-danger">
			<p>{{ $message }}</p>
		</div>
	@endif
	<div class="div-bg-gray ">
		<div class="card-body">
			<table id="key-table" style="width: 100%;"
			       class="table table-striped table-hover dataTable border-table">
				<thead>
				@if($filter != 1)
					<th>ENTIDADE</th>
				@endif
				<th>DOCUMENTO</th>
				<th>ESTADO</th>
				<th>VALIDADE</th>
				@can('administrator')
					<th>AÇÃO</th>
				@endcan

				</thead>
				<tbody>
				@foreach($docs as $doc)
					@if($user->enterprise->enterpriseID == $doc->company_id or $user->enterprise->enterpriseID == 1)
						<tr>
							@if($filter != 1)
								<td>
									<div class="pd5-all">{{$doc->enterprise->enterpriseName}}</div>
								</td>
							@endif
							<td>
								<div class="pd5-all">
									<form method="post" action="{{route('company.document.get')}}">
										@csrf
										<input type="hidden" id="doc" name="doc"
										       value="{{$doc->documentName}}.{{$doc->document_extension}}">
										<input type="hidden" id="id" name="id" value="{{$doc->company_id}}">
										<input type="hidden" id="hash" name="hash"
										       value="@php echo(Illuminate\Support\Facades\Hash::make( \Carbon\Carbon::today() && $doc->documentName && $user->email && $user->enterprise->enterpriseName));@endphp">
										<button class="btn"
										        style="padding-top: 3px;padding-bottom: 3px;">@if($doc->document_extension == 'pdf')
												<i class="far fa-file-pdf"></i>&ensp;{{$doc->types->typeName}}
											@elseif($doc->document_extension == 'docx')
												<i class="far fa-file-word"></i>
												&ensp;{{$doc->types->typeName}}
											@elseif($doc->document_extension == 'docx')
												<i class="far far fa-file-excel"></i>
												&ensp;{{$doc->types->typeName}}
											@else
												<i class="far far fa-file"></i>
												&ensp;{{$doc->types->typeName}}

											@endif</button>
									</form>
								</div>
							</td>
							<td>
								<div class="pd5-all">
									@php
										$valid= $doc->valid;
										$expires_at = $doc->expires_at;

									if($valid == 2) {
									echo('<label data-toggle="tooltip" title="'.$doc->description .'" class="badge badge-danger  text-16"><i class="fas fa-times-circle fa-1x"></i></label>');
									 }else if($expires_at < Carbon\Carbon::today('Europe/Lisbon') && $expires_at != NULL ){
								   echo('<label data-toggle="tooltip" title="Documento Expirado" class="badge badge-danger  text-16"><i class="fas fa-times-circle fa-1x"></i></label>');
									   }
									else if($valid == 0)
									{
									echo('<label class="badge badge-warning  text-16"><i class="fas fa-exclamation-circle fa-1x"></i></label>');
									   }else if ($valid ==1 && $expires_at === NULL ){
						   echo('<label class="badge badge-success pd5-all text-16"><i class="fas fa-check fa-1x"></i></label>');
						   }else{
						   echo('<label class="badge badge-warning pd5-all text-16"><i class="fas fa-check
						   fa-1x"></i></label>');
						   }

									@endphp
								</div>

							</td>
							<td>
								<div class="pd5-all">

									@php
										$expire= $doc->expires_at;

									iF($expire == NULL)
									{
									 echo('<label class="badge"><i class="fas fa-ban fa-2x"></i></label>');
									}else if($expire < Carbon\Carbon::today('Europe/Lisbon') && $expires_at != NULL){
									echo('<label class="badge text-16">'.$expire.'</label>');
									}else if($expire < Carbon\Carbon::today('Europe/Lisbon')->addMonth(1) && $expires_at != NULL){
									echo('<label class="badge text-16">'.$expire.'</label>');
									}
									else{
									echo('<label class="badge text-16">'.$expire.'</label>');
									}
									@endphp
								</div>

							</td>
							@can('administrator')
								<td>
									<div class="row">

										@if(Auth::user()->enterprise->enterpriseID ==1)
											<div class="pd5-all">
												<form method="post" action="{{route('company.document.approve')}}">
													@csrf
													<input type="hidden" id="doc" name="doc"
													       value="{{$doc->documentName}}">
													<button style="padding-top: 3px;padding-bottom: 3px;"
													        class="btn btn-success"><i
																class="far fa-thumbs-up fa-1x"></i>
													</button>
												</form>
											</div>
											<div class="pd5-all">
												<form id="deny{{$doc->documentName}}.{{$doc->document_extension}}"
												      method="post"
												      action="{{route('company.document.deny')}}">
													@csrf
													<input type="hidden" id="doc" name="doc"
													       value="{{$doc->documentName}}">

												</form>
												<button style="padding-top: 3px;padding-bottom: 3px;"
												        class="btn btn-danger"
												        onclick="deny('deny{{$doc->documentName}}.{{$doc->document_extension}}');">
													<i class="far fa-thumbs-down fa-1x"></i>
												</button>
											</div>
											<div class="pd5-all">
												<input type="hidden"
												       id="reason_deny{{$doc->documentName}}.{{$doc->document_extension}}"
												       placeholder="Motivo de Rejeição"
												       form="deny{{$doc->documentName}}.{{$doc->document_extension}}"
												       class="form-control" type="text" id="desc" name="desc">
											</div>
										@endif
									</div>
								</td>
							@endcan
						</tr>
					@endif
				@endforeach
				</tbody>
			</table>

			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12">
					<div class="pull-left">
						<h4>Legenda</h4>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-2">
					<div>
						<strong>
							<a class="border300 btn btn-success"><i class="fa25 fas fa-check fa-1x"></i></a>&nbsp;Válido
						</strong>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-2">
					<div>
						<strong>
							<a class="border300 btn btn-warning"><i class="fa25 fas fa-check fa-1x"></i></a>&nbsp;
							Validade Reduzida
						</strong>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-2">
					<div>
						<strong>
							<a class="border300 btn btn-warning"><i class="fa25 fas
							fa-exclamation-circle"></i></a>&nbsp;Pendente
						</strong>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-2">
					<div>
						<strong>
							<a class="border300 btn btn-danger"><i class="fa25 fas fa-times-circle"></i></a>&nbsp;Inválido
						</strong>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-2">
					<div>
						<strong>
							<a class="border300 btn btn-info"><i class="fa25 fas fa-plus-circle"></i></a>&nbsp;Vazio
						</strong>
					</div>
				</div>
			</div>
		</div>
	</div>


@endsection
@section('script')

	<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.18/r-2.2.2/datatables.min.js"></script>
	<script type="text/javascript"
	        src="https://cdn.datatables.net/responsive/2.2.2/js/dataTables.responsive.min.js"></script>
	<script type="text/javascript"
	        src="https://cdn.datatables.net/responsive/2.2.2/js/responsive.bootstrap4.min.js"></script>
	<script type="text/javascript" src="{{asset('js/sweetalert2.all.min.js')}}"></script>
	<script>
        $(document).ready(function () {
            const table = $('#key-table').DataTable({
                responsive: true,
                bInfo: false,
                bLengthChange: false,
                order: [0, 'desc'],
                stateSave: true,
                "language": {
                    "sEmptyTable": "Nenhum registo encontrado",
                    "sProcessing": "A processar...",
                    "sLengthMenu": "Mostrar _MENU_ registos",
                    "sZeroRecords": "Não foram encontrados resultados",
                    "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registos",
                    "sInfoEmpty": "Mostrando de 0 até 0 de 0 registos",
                    "sInfoFiltered": "(filtrado de _MAX_ registos no total)",
                    "sInfoPostFix": "",
                    "sSearch": "Procurar:",
                    "sUrl": "",
                    "oPaginate": {
                        "sFirst": "Primeiro",
                        "sPrevious": "Anterior",
                        "sNext": "Seguinte",
                        "sLast": "Último"
                    },
                    "oAria": {
                        "sSortAscending": ": Ordenar colunas de forma ascendente",
                        "sSortDescending": ": Ordenar colunas de forma descendente"
                    }
                }
            });
            $("#all").click(function () {
                table.search("").draw();
            });
            $("#mine").click(function () {
                table.search("{{$user->enterprise->enterpriseName}}").draw();
            });
        });


	</script>
	<script>
        $(document).ready(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });

        function deny(id) {
            (async function getText() {
                const {value: text} = await swal({
                    input: 'textarea',
                    title: 'Motivo',
                    inputPlaceholder: 'Motivo de rejeição',
                    showCancelButton: true
                });

                if (text) {

                    var element = document.getElementById('reason_' + id);
                    element.value = text;

                    document.getElementById(id).submit();
                }

            })()
        }
	</script>

@endsection