@extends('layouts.app')

@section('main')
	<div class="row">
		<div class="col-lg-12 margin-tb">
			<div class="pull-left">
				<h2>Adicionar tipo de documento</h2>
			</div>
			<div class="pull-right ">
				<a class="btn btn-primary" href="{{ route('company.index') }}"> Voltar</a>
			</div>
		</div>
	</div>

	@if (count($errors) > 0)
		<div class="alert alert-danger">
			<strong>Whoops!</strong> There were some problems with your input.<br><br>
			<ul>
				@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
	@endif

	<form action="{{route('company.document.typecreate')}}" method="post"
	      enctype="multipart/form-data">
		@csrf
		<div class="row">
			<input type="hidden" name="id" value="{{$user->id}}">
			<div class="col-xs-12 col-sm-12 col-md-12">
				<div class="form-group">
					<strong>Nome Do Documento:</strong>
					<input class="form-control" type="text" name="name" id="name">
				</div>
				<div class="form-group">
					<strong>Documento Expira:</strong>
					<div>
						<label class="switch">
							<input name="expires" id="expires" type="checkbox">
							<span class="slider round"></span>
						</label>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-12 text-center">
				<button id="submit" type="submit" class="btn btn-primary">Submeter</button>
			</div>
		</div>
	</form>

@endsection
@section('script')
	<script>
        function makeid() {
            var text = "";
            var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

            for (var i = 0; i < 7; i++)
                text += possible.charAt(Math.floor(Math.random() * possible.length));

            document.getElementById('password').value = text;
            document.getElementById('password_conf').value = text;
        }
	</script>

@endsection