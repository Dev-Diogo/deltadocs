@extends('layouts.app')
@section('css')
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.css"/>
	<link rel="stylesheet" type="text/css"
	      href="https://cdn.datatables.net/responsive/2.2.2/css/responsive.bootstrap4.min.css"/>

@endsection
@section('main')
	<div class="row">
		<div class="col-lg-12 margin-tb">
			<div class="pull-left">
				<h2>Roles</h2>
			</div>
			<div class="pull-right">
				@can('role-create')
					<a class="btn btn-success" href="{{ route('roles.create') }}"> Create Role</a>
				@endcan
			</div>
		</div>
	</div>

	@if ($message = Session::get('success'))
		<div class="alert alert-success">
			<p>{{ $message }}</p>
		</div>
	@endif
	<div class="div-bg-gray ">
		<div class="card-body">
			<table id="key-table" class="table table-bordered">
				<thead>
				<th>ROLE ID</th>
				<th>ROLE NAME</th>
				<th>ROLE PERMISSIONS</th>
				<th>ACTION MENU</th>
				</thead>
				<tbody>
				@foreach ($roles as $key => $role)
					<tr>
						<td>{{ ++$i }}</td>
						<td><label style="font-size: 16px" class="badge badge-success">{{ $role->name }}</label></td>
						<td><a class="btn btn-info" href="{{ route('roles.show',$role->id) }}">Show</a>
						</td>
						<td>

							@can('role-edit')
								@if($role->id == 1)
									@hasrole('Root')
									<a class="btn btn-primary" href="{{ route('roles.edit',$role->id) }}">Edit</a>
								@else
									<a class="btn btn-primary disabled"
									   href="{{ route('roles.edit',$role->id) }}">Edit</a>
									@endhasrole
									@else
										<a class="btn btn-primary" href="{{ route('roles.edit',$role->id) }}">Edit</a>
									@endif
								@endcan
								@can('role-delete')
									{!! Form::open(['method' => 'DELETE','route' => ['roles.destroy', $role->id],'style'=>'display:inline']) !!}
									{!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
									{!! Form::close() !!}
								@endcan
						</td>
						</td>
					</tr>
				@endforeach
				</tbody>
			</table>
		</div>
	</div>

@endsection
@section('script')

	<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.18/r-2.2.2/datatables.min.js"></script>
	<script type="text/javascript"
	        src="https://cdn.datatables.net/responsive/2.2.2/js/dataTables.responsive.min.js"></script>
	<script type="text/javascript"
	        src="https://cdn.datatables.net/responsive/2.2.2/js/responsive.bootstrap4.min.js"></script>

	<script>
        $(document).ready(function () {
            $('#key-table').DataTable({
                responsive: true
            });
        });
	</script>
@endsection
